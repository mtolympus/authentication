# Hermes Authentication

A feature rich authentication package for Laravel 5.7+. Meant to enable developers to have an advanced authentication system up and running in a few minutes.
Makes as much use of 3rd party packages, which are all awesome, instead of re-inventing the wheel.

## Features

- Registration
- Account recovery
- Email verification
- Two factor authentication
- Emails, Jobs and Events
- Middleware
- Roles & permissions
- Fully customizable/replacable UI
- Integration with the Hermes\Admin package out of the box

## Backend dependencies

- [Laracasts\flash](https://github.com/laracasts/flash) ```^3.0```
- [Hermes\settings](https://bitbucket.org/mtolympus/settings) ```1.0.0```
- [Webpatser\laravel-uuid](https://github.com/webpatser/laravel-uuid) ```^3.0```
- [Spatie\laravel-permission](https://github.com/spatie/laravel-permission) ```2.*```
- [Cviebrock\eloquent-sluggable](https://github.com/cviebrock/eloquent-sluggable) ```^4.6```
- [Pragmarx\Google2fa-laravel](https://github.com/antonioribeiro/google2fa-laravel) ```^0.2.0```
- [Bacon\Bacon-qr-code](https://github.com/Bacon/BaconQrCode) ```1.0.3```

## Frontend dependencies

- [Vue](https://vuejs.org/)
- [Vuetify](https://vuetifyjs.org)
- [Fontawesome](https://fontawesome.com)