<!-- _coverpage.md -->

![logo](_media/logo-black.png)

# Hermes Auth Package<small>v1.1.0</small>

> Advanced authentication in 1 minute

- Registration
- Account recovery
- Email verification
- Two factor authentication
- Emails
- Events
- Fully customizable (& replaceable) UI
- Integration with the Admin panel package

[Get Started](#get-started)