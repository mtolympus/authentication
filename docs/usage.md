# Usage instructions

## Helper methods

Because we could not make calls to the route() method within config files we needed a way to render them using the config file to pass the data to wherever it was needed.
We made a simple method available which helps us parse the (redirection) target's data and return the desired url.

```php
/**
 * Parse redirect target
 * 
 * @param       array           Array with details of the target route
 * @return      string          The parsed URL
 */
public function parse_redirect_target(array $data)
```

## Available routes

The auth package makes the following routes available:

| Method | URI                                | Name                               | Action                                         | Middleware |
|--------|------------------------------------|------------------------------------|------------------------------------------------|------------|
| GET    | /login                             | auth.login                         | AuthController@getLogin                        | web,guest  |
| POST   | /login                             | auth.login.post                    | AuthController@postLogin                       | web,guest  |
| GET    | /register                          | auth.register                      | AuthController@getRegister                     | web,guest  |
| POST   | /register                          | auth.register.post                 | AuthController@postRegister                    | web,guest  |
| GET    | /recover-password                  | auth.recover                       | AuthController@getRecover                      | web,guest  |
| POST   | /recover-password                  | auth.recover.post                  | AuthController@postRecover                     | web,guest  |
| GET    | /reset-password                    | auth.reset-password                | AuthController@getResetPassword                | web,guest  |
| POST   | /reset-password                    | auth.reset-password.post           | AuthController@postResetPassword               | web,guest  |
| GET    | /verify-email/{email}/{code}       | auth.verify-email                  | AuthController@getVerifyEmail                  | web        |
| GET    | /resend-verification-email/{email} | auth.resend-verification-email     | AuthController@getResendVerificationEmail      | web        |
| GET    | /verification-email-sent/{email}   | auth.verify-email.sent             | AuthController@getVerificationEmailSent        | web        |
| GET    | /setup-2fa                         | auth.2fa-setup                     | TwoFactorController@getSetup                   | web,auth   |
| POST   | /setup-2fa                         | auth.2fa-setup.post                | TwoFactorController@postSetup                  | web,auth   |
| GET    | /2fa                               | auth.2fa                           | TwoFactorController@getAuthenticate            | web,auth   |
| POST   | /2fa                               | auth.2fa.post                      | TwoFactorController@postAuthenticate           | web,auth   |
| GET    | /2fa-recovery                      | auth.2fa.recovery                  | TwoFactorController@getAuthenticateRecovery    | web,auth   |
| POSt   | /2fa-recovery                      | auth.2fa.recovery.post             | TwoFactorController@postAuthenticateRecovery   | web,auth   |
| GET    | /logout                            | auth.logout                        | AuthController@getLogout                       | web,auth   |
| POST   | /api/resend-verification-email     | api.auth.resend-verification-email | Api\AuthController@postResendVerificationEmail | api        |

All controllers should be prepended with the `Hermes\Auth\Http\Controllers\` namespace.

## Available middleware

The following middlewares are made avaible for you to use:

- `auth` - User should be authenticated
- `auth.full` - User should be authenticated + two factor authenticated if required
- `guest` - User should not be authenticated
- `admin` - User should have the administrator role

## Vue components

The following Vue components are published by the package:

### Remember me input

A simple form element which is displayed on the login page, indicating whether the user wants to be remember by the app.

```html
<remember-me-input
    name="fieldName">
</remember-me-input>
```

Parameters:
    - `name` the name of the input field, which will be the key within the request's input (post data)

### Resend verification email button

A dynamic button which is displayed on the verify your email page. When clicked it will display a loading icon.

```html
<resend-verification-email-button
    user-uuid=""
    button-text=""
    api-endpoint=""
    redirect-endpoint="">
</resend-verification-email-button>
```

Parameters:
    - `userUuid` the UUID of the user, which we need for the API request payload
    - `buttonText` the text to display on the button
    - `apiEndpoint` the API endpoint which we should send the 'resend-verification-email' payload to
    - `redirectEndpoint` where we should redirect the user after a succesful API request

### Two factor setup

The form to display on the two-factor authentication setup page. It divides the 3 steps into a little wizard.

```html
<two-factor-setup
    qr-image-url=""
    old-input="">
</two-factor-setup>
```

Parameters:
    - `qrImageUrl` the data representing the QR code the user needs to scan (generated by the bacon package)
    - `oldInput` a JSON encoded Collection of old input fields (the code)

### Admin panel module

#### Users

#### Roles

#### Permissions
