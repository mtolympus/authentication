# Installation instructions

## Preparing your project

Laravel places all of it's models in the `app` directory by default. In our experience this is not the best place to store models since this folder will get rather 
large as the application grows. Instead I like to create a directory called `app/Models` in which I place all of my models.

### Create the Models directory

So go ahead and create the `app/Models` directory as well. Afterwards update your `config/auth.php` file on line #70 and set the `model` property to `App\Models\User::class`.

### The User model

#### Replace your User model

Delete your current `app/User.php` model and create a new model in your fresh Models directory called `App\Models\User.php` and copy-paste the following:

```php
<?php

namespace App\Models;

use Hermes\Auth\Models\User as BaseUser;

class User extends BaseUser
{
    protected $fillable = [
        "uuid",
        'annotation',
        'name',
        'email',
        'email_verified',
        'email_verified_at',
        'email_verification_code',
        'password',
        'password_recovery_code',
    ];
}
```

#### Delete your user table migration

Delete the default `create_users_table.php` migration as this one will be replaced after publishing the Auth package's assets.

#### The home route

The authentication package requires you to have a named route called `home`.

## Installing frontend dependencies

### Fontawesome

Font awesome is included with a CDN include on the master layout of the auth package. You could remove this include and move it to the stylesheets.

### Vuetify

Run the following command to install the awesome vuetify frontend library:
```
npm install vuetify --save
```

After installation is complete open up your `resources/js/app.js` file and add the following to load Vuetify:
```js
// Load vuetify
import Vuetify from 'vuetify'
Vue.use(Vuetify)
```

We recommend you to create a `_general.scss` stylesheet which you load into each of your template's individual `.scss` files. 
This way you don't have to redefine your common code. Add the following to your `_general.scss` or `auth.scss` file to load Vuetify's styling:

```scss
// Vuetify
@import '~vuetify/dist/vuetify.min.css';
```

### Vue components

Make sure the vue components are being loaded. Either load them manually in your `app.js` file or if you're using the recursive loading script that 
was added to the latest Laravel distribution you can skip this step.

### Stylesheets

Add the `auth.scss` file that was published to your `webpack.mix.js` file. Which should look something like:

```js
let mix = require('laravel-mix');
let webpack = require('webpack');

mix.js('resources/js/app.js', 'public/js');

mix.sass('resources/sass/app.scss', 'public/css')
    .sass('resources/sass/auth.scss', 'public/css');

mix.disableNotifications()
    .browserSync({
        proxy: 'auth-demo.test',
        port: 7777
    });

mix.webpackConfig({
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        })
    ]
});
```

## Installing the package

Enter the application's root directory in your terminal and run the following command to install the package and it's dependencies:
```
composer require hermes/auth
```

### Publish assets

Afterwards run the following command to publish the package's assets:
```
php artisan vendor:publish --provider="Hermes\Auth\Providers\AuthServiceProvider"
```

The auth package depends on a few other packages which require some of their assets being published as well. So run the following commands as well:
```
php artisan vendor:publish --provider="Hermes\Settings\Providers\SettingsServiceProvider"
php artisan vendor:publish --provider="PragmaRX\Google2FALaravel\ServiceProvider"
```

### Setup the database

#### Seeders

Register the two published seeders in your `database\seeds\DatabaseSeeder.php` file:

```php
<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SettingSeeder::class);
        $this->call(AuthSeeder::class);
    }
}
```

Afterwards run the following command so the Seeders can actually be found by the application:
```
composer dumpautoload
```

#### Migrate the database

If you're running on a fresh installation and you did not run any migrations yet; run the following command:

```
php artisan migrate --seed
```

If you have ran migrations already run the following command:
```
php artisan migrate && php artisan migrate:refresh --seed
```

### Update application's middleware

Open up your `app/Http/Kernel.php` file and delete the default middlewares for authentication:
```php
'auth' => \App\Http\Middleware\Authenticate::class,
'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
```

The authentication package automatically loads 4 new middlewares for you to use:
- `auth` (User should be authenticated)
- `auth.full` (User should be authenticated + two factor authenticated if required)
- `guest` (User should not be authenticated)
- `admin` (User should have the administrator role)

**TODO** Add the role/permission related middlewares from the spatie package.

## Configure the auth package

When you published the package's assets a config file called `config\authentication.php` was created. You can edit that to make the desired changes in behaviour and content.

Whenever routes are involved in the configuration they will have a specific format. We can't render the routes within the config file for some reason so we will have to do it using a helper method in the controllers where we need them. More information on this can be found in the usage section.

A route is formatted as such:
```php
'redirect' => ['type' => '', 'target' => '', 'params' => ['key' => 'val']],
```

Allowed types are:
- `named_route` which accepts the name of a named route as it's target along with any required parameters
- `url` which accepts an URL as it's target