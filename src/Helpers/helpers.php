<?php

/**
 * Parse redirect target
 * 
 * This method is responsible for converting the redirect targets defined in our config file to an URL.
 * We can't call the route() method in our config file so we're doing it this way.
 *
 * @param       array                   Array with details of the target route
 * @return      string                  URL of the target route
 */

if (!function_exists("parse_redirect_target"))
{
    function parse_redirect_target(array $data)
    {
        if (array_key_exists("type", $data) and array_key_exists("target", $data))
        {
            switch ($data["type"])
            {
                default:
                case "url":
                    return $data["target"];
                break;
                
                case "named_route":
                    if (array_key_exists("params", $data))
                    {
                        return route($data["target"], $data["params"]);
                    }
                    else
                    {
                        return route($data["target"]);
                    }
                break;
            }
        }

        return "/";
    }
}

// // If the target variable is an array as request
// if (is_array($target))
// {
//     // Make sure we got a type and target key in the target data array
//     if (array_key_exists("type", $target) and array_key_exists("target", $target))
//     {
//         // Switch between supported target types
//         switch ($target["type"])
//         {
//             // Default and just hardcoded urls
//             default:
//             case "url":
//                 // Get their raw targets returned
//                 return $target["target"];
//             break;
//             case "named_route":
//                 // If the array contains additionals paramaters
//                 if (array_key_exists("params", $target))
//                 {
//                     // Return the named route location using the desired parameters
//                     return route($target["target"], $target["params"]);
//                 }
//                 // If we only received a target
//                 else
//                 {
//                     // Return the named route location
//                     return route($target["target"]);
//                 }
//             break;
//         }
//     }
// }
// // In all other cases we return the root of the website (the assumed homepage)
// return "/";