@extends("auth::layouts.master")

@section("page_title")
    - Register
@stop

@section("content")

    <!-- Auth content card -->
    <div id="auth-content-card" class="elevation-24 @if(!$has_panel) no-panel @endif">

        <!-- Card left side -->
        @if ($has_panel)
            <div id="auth-content-card__left">
                <div id="auth-content-card__cinematic">
                    <div id="card-title">{{ $panel_title }}</div>
                    <div id="card-subtitle">{{ $panel_text }}</div>
                </div>
            </div>
        @endif

        <!-- Card right side -->
        <div id="auth-content-card__right">

            <!-- Form panel -->
            <div id="auth-content-card__form">
                
                <!-- Titles -->
                <h1 id="form-title">Register</h1>
                <div id="form-subtitle">Create your personal account in less than a minute.</div>

                <!-- Feedback -->
                @include($feedbackPartial)
                
                <!-- The form -->
                <form action="{{ route('auth.register.post') }}" method="post">
                    {{ csrf_field() }}
                    
                    <!-- Name -->
                    <v-text-field label="Name" name="name"></v-text-field>

                    <!-- Email -->
                    <v-text-field label="Email" name="email"></v-text-field>

                    <!-- Password -->
                    <v-text-field label="Password" name="password" type="password"></v-text-field>

                    <!-- Confirm password -->
                    <v-text-field label="Confirm password" name="password_confirmation" type="password"></v-text-field>
                    
                    <!-- Form controls -->
                    <div class="auth-form-controls margin-top">
                        <div class="auth-form-controls__left">
                            <v-btn flat small href="{{ route('auth.login') }}">
                                Already have an account? Login
                            </v-btn>
                        </div>
                        <div class="auth-form-controls__right">
                            <v-btn type="submit" color="primary">
                                Create your account!
                            </v-btn>
                        </div>
                    </div>

                </form>

            </div><!-- End of form panel -->

        </div><!-- End of card right side -->

    </div><!-- End of auth content card -->

@stop
