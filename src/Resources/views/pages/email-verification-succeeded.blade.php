@extends("auth::layouts.master")

@section("page_title")
    - Email verified
@stop

@section("content")
    <div id="auth-content-card">
        <div id="auth-content-card__content">
            <div id="auth-content-card__message">
                <h1 id="message-title">
                    Your email address has been verified!
                </h1>
                <div id="message-text">
                    Awesome stuff bro(dette); may the force be with you.
                </div>
                <div id="message-icon">
                    <i class="fas fa-grin-beam"></i>
                </div>
                <div id="message-actions">
                    <v-btn color="primary" large flat href="{{ $button_href }}">
                        {{ $button_text }}
                    </v-btn>
                </div>
            </div>
        </div>
    </div>
@stop