@extends("admin::layouts.master")

@section("content")
    <div class="wrapper">

        <!-- Breadcrumbs -->
        {!! Breadcrumbs::render("admin.permissions") !!}

        <!-- Header -->
        <div class="page-header">
            <div class="page-header__text">
                <div class="page-header__title">Manage permissions</div>
                <div class="page-header__subtitle">There must be rules.</div>
            </div>
            <div class="page-header__actions"></div>
        </div>
    
        <!-- Feedback -->
        @include("admin::partials.feedback")

        <!-- Content -->
        <permission-overview
            :roles="{{ $roles->toJson() }}"
            :permission-groups="{{ $permissionGroups->toJson() }}">
        </permission-overview>

    </div>
@stop