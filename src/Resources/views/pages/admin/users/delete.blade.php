@extends("admin::layouts.master")

@section("content")
    <div class="wrapper">

        <!-- Breadcrumbs -->
        {!! Breadcrumbs::render("admin.users.delete", $user) !!}

        <!-- Header -->
        <div class="page-header">
            <div class="page-header__text">
                <div class="page-header__title">Delete user</div>
            </div>
        </div>
    
        <!-- Feedback -->
        @include("admin::partials.feedback")

        <!-- Content -->
        <form action="{{ route('admin.users.delete', $user->slug) }}" method="post">
            {{ csrf_field() }}
            
            <div class="content-card elevation-1">
                Are you sure you want to delete this user '{{ $user->annotation }} {{ $user->name }}'?
            </div>

            <div class="controls">
                <div class="controls-left">
                    <v-btn color="primary" href="{{ route('admin.users.view', $user->slug) }}">
                        <i class="fas fa-long-arrow-alt-left"></i>
                        No, back to user
                    </v-btn>
                </div>
                <div class="controls-right">
                    <v-btn dark color="red" type="submit">
                        <i class="far fa-trash-alt"></i>
                        Yes, delete user
                    </v-btn>
                </div>
            </div>

        </form>

    </div>
@stop