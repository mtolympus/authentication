@extends("admin::layouts.master")

@section("content")
    <div class="wrapper">

        <!-- Breadcrumbs -->
        {!! Breadcrumbs::render("admin.users.create") !!}

        <!-- Header -->
        <div class="page-header">
            <div class="page-header__text">
                <div class="page-header__title">Create a new user</div>
            </div>
        </div>
    
        <!-- Feedback -->
        @include("admin::partials.feedback")

        <!-- Content -->
        <form action="{{ route('admin.users.create.post') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="content-card no-padding elevation-1">
                <user-form
                    :roles="{{ $roles->toJson() }}"
                    :permissions="{{ $permissions->toJson() }}"
                    :permission-groups="{{ $permissionGroups->toJson() }}"
                    :old-input="{{ $oldInput->toJson() }}">
                </user-form>
            </div>
            
            <div class="controls">
                <div class="controls-left">
                    <v-btn color="primary" href="{{ route('admin.users') }}">
                        <i class="fas fa-long-arrow-alt-left"></i>
                        Back to overview
                    </v-btn>
                </div>
                <div class="controls-right">
                    <v-btn color="success" type="submit">
                        <i class="far fa-save"></i>
                        Save user
                    </v-btn>
                </div>
            </div>

        </form>

    </div>
@stop