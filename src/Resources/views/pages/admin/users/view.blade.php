@extends("admin::layouts.master")

@section("content")
    <div class="wrapper">

        <!-- Breadcrumbs -->
        {!! Breadcrumbs::render("admin.users.view", $user) !!}

        <!-- Header -->
        <div class="page-header">
            <div class="page-header__text">
                <div class="page-header__title">View user</div>
            </div>
            <div class="page-header__actions">
                <v-btn small color="warning" dark href="{{ route('admin.users.edit', $user->slug) }}">
                    <i class="far fa-edit"></i>
                    Edit
                </v-btn>
                <v-btn small color="red" dark href="{{ route('admin.users.delete', $user->slug) }}">
                    <i class="far fa-trash-alt"></i>
                    Delete
                </v-btn>
            </div>
        </div>
    
        <!-- Feedback -->
        @include("admin::partials.feedback")

        <!-- Content -->
        <div class="content-card no-padding elevation-1">
            <div class="details">
                <div class="detail">
                    <div class="key">ID</div>
                    <div class="val">{{ $user->id }}</div>
                </div>
                <div class="detail">
                    <div class="key">UUID</div>
                    <div class="val">{{ $user->uuid }}</div>
                </div>
                <div class="detail">
                    <div class="key">Roles</div>
                    <div class="val">
                        @if ($user->roles->count() > 0)
                            <role-pills>
                                @foreach ($user->roles as $role)
                                    <a href="{{ route('admin.roles.view', $role->id) }}">
                                        <role-pill :role="{{ $role->toJson() }}"></role-pill>
                                    </a>
                                @endforeach
                            </role-pills>
                        @else
                            <i>No roles have been assigned to this user</i>
                        @endif
                    </div>
                </div>
                <div class="detail">
                    <div class="key">Name</div>
                    <div class="val">{{ $user->annotation }} {{ $user->name }}</div>
                </div>
                <div class="detail">
                    <div class="key">Email</div>
                    <div class="val">{{ $user->email }}</div>
                </div>
                <div class="detail">
                    <div class="key">Email verified</div>
                    <div class="val">
                        @if ($user->email_verified)
                            Yes <i>({{ $user->email_verified_at}})</i>
                        @else
                            No
                        @endif
                    </div>
                </div>
                <div class="detail">
                    <div class="key">Created at</div>
                    <div class="val">{{ $user->created_at }}</div>
                </div>
                <div class="detail">
                    <div class="key">Updated at</div>
                    <div class="val">{{ $user->updated_at }}</div>
                </div>
            </div>
        </div>

        <!-- Permissions -->
        <div class="content-card no-padding elevation-1">
            <user-permissions
                :user="{{ $user->toJson() }}"
                :permissions="{{ $user->permissions->toJson() }}"
                :permission-groups="{{ $permissionGroups->toJson() }}">
            </user-permissions>
        </div>

        <!-- Controls -->
        <div class="controls">
            <div class="controls-left">
                <v-btn color="primary" href="{{ route('admin.users') }}">
                    <i class="fas fa-long-arrow-alt-left"></i>
                    Back to overview
                </v-btn>
            </div>
        </div>

    </div>
@stop