@extends("admin::layouts.master")

@section("content")

    <div class="wrapper">

        <!-- Breadcrumbs -->
        {!! Breadcrumbs::render("admin.users") !!}

        <!-- Header -->
        <div class="page-header">
            <div class="page-header__text">
                <div class="page-header__title">Manage users</div>
                <div class="page-header__subtitle">Herd the sheep m'lord.</div>
            </div>
            <div class="page-header__actions">
                <v-btn small color="primary" href="{{ route('admin.users.create') }}">
                    <i class="fas fa-plus"></i>
                    Create new user
                </v-btn>
            </div>
        </div>
    
        <!-- Feedback -->
        @include("admin::partials.feedback")

        <!-- Content -->
        <data-table
            :data="{{ $users->toJson() }}"
            :config="{{ $dataTableConfig }}">
        </data-table>

    </div>

@stop