@extends("admin::layouts.master")

@section("content")
    <div class="wrapper">

        <!-- Breadcrumbs -->
        {!! Breadcrumbs::render("admin.roles.view", $role) !!}

        <!-- Header -->
        <div class="page-header">
            <div class="page-header__text">
                <div class="page-header__title">View role</div>
            </div>
            <div class="page-header__actions">
                <v-btn small color="warning" dark href="{{ route('admin.roles.edit', $role->id) }}">
                    <i class="far fa-edit"></i>
                    Edit
                </v-btn>
                <v-btn small color="red" dark href="{{ route('admin.roles.delete', $role->id) }}">
                    <i class="far fa-trash-alt"></i>
                    Delete
                </v-btn>
            </div>
        </div>
    
        <!-- Feedback -->
        @include("admin::partials.feedback")

        <!-- Content -->
        <div class="content-card no-padding elevation-1">
            <div class="details">
                <div class="detail">
                    <div class="key">ID</div>
                    <div class="val">{{ $role->id }}</div>
                </div>
                <div class="detail">
                    <div class="key">Name</div>
                    <div class="val">{{ $role->name }}</div>
                </div>
                <div class="detail">
                    <div class="key">Icon</div>
                    <div class="val">
                        @if ($role->icon == "")
                            <i>This role does not have an icon</i>
                        @else
                            {!! $role->icon !!}
                        @endif
                    </div>
                </div>
                <div class="detail">
                    <div class="key">Text color</div>
                    <div class="val">
                        <color-display color="{{ $role->text_color }}"></color-display>
                        {{ strtoupper($role->text_color) }}
                    </div>
                </div>
                <div class="detail">
                    <div class="key">Background color</div>
                    <div class="val">
                        <color-display color="{{ $role->bg_color }}"></color-display>
                        {{ strtoupper($role->bg_color) }}
                    </div>
                </div>
                <div class="detail">
                    <div class="key">Created at</div>
                    <div class="val">{{ $role->created_at }}</div>
                </div>
                <div class="detail">
                    <div class="key">Updated at</div>
                    <div class="val">{{ $role->updated_at }}</div>
                </div>
            </div>
        </div>

        <!-- Permissions -->
        <div class="content-card no-padding elevation-1">
            <permission-list
                :role-permissions="{{ $role->permissions->toJson() }}"
                :permission-groups="{{ $permissionGroups->toJson() }}">
            </permission-list>
        </div>

        <!-- Controls -->
        <div class="controls">
            <div class="controls-left">
                <v-btn color="primary" href="{{ route('admin.roles') }}">
                    <i class="fas fa-long-arrow-alt-left"></i>
                    Back to overview
                </v-btn>
            </div>
        </div>

    </div>
@stop