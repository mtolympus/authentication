@extends("admin::layouts.master")

@section("content")
    <div class="wrapper">

        <!-- Breadcrumbs -->
        {!! Breadcrumbs::render("admin.roles.edit", $role) !!}

        <!-- Header -->
        <div class="page-header">
            <div class="page-header__text">
                <div class="page-header__title">Edit role</div>
            </div>
        </div>
    
        <!-- Feedback -->
        @include("admin::partials.feedback")

        <!-- Content -->
        <form action="{{ route('admin.roles.edit.post', $role->id) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            
            <!-- Role form -->
            <div class="content-card elevation-1">
                <role-form :role="{{ $role->toJson() }}" :old-input="{{ $oldInput->toJson() }}"></role-form>
            </div>

            <!-- Permission subheader -->
            <div class="content-card-header">
                <div class="content-card-header__text">
                    <div class="content-card-header__title">Select role's permissions</div>
                </div>
            </div>

            <!-- Permissions -->
            <div class="content-card no-padding elevation-1">
                <permissions-form 
                    :role="{{ $role->toJson() }}"
                    :permissions="{{ $permissions->toJson() }}"
                    :permission-groups="{{ $permissionGroups->toJson() }}"
                    :old-input="{{ $oldInput->toJson() }}">
                </permissions-form>
            </div>

            <!-- Controls -->
            <div class="controls">
                <div class="controls-left">
                    <v-btn color="primary" href="{{ route('admin.roles') }}">
                        <i class="fas fa-long-arrow-alt-left"></i>
                        Back to overview
                    </v-btn>
                </div>
                <div class="controls-right">
                    <v-btn color="success" type="submit">
                        <i class="far fa-save"></i>
                        Save changes
                    </v-btn>
                </div>
            </div>

        </form>

    </div>
@stop