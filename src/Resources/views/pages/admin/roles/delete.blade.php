@extends("admin::layouts.master")

@section("content")
    <div class="wrapper">

        <!-- Breadcrumbs -->
        {!! Breadcrumbs::render("admin.roles.delete", $role) !!}

        <!-- Header -->
        <div class="page-header">
            <div class="page-header__text">
                <div class="page-header__title">Delete role</div>
            </div>
        </div>
    
        <!-- Feedback -->
        @include("admin::partials.feedback")

        <!-- Content -->
        <form action="{{ route('admin.roles.delete', $role->id) }}" method="post">
            {{ csrf_field() }}
            
            <div class="content-card elevation-1">
                Are you sure you want to delete this role '{{ $role->name }}'?
            </div>

            <div class="controls">
                <div class="controls-left">
                    <v-btn color="primary" href="{{ route('admin.roles.view', $role->id) }}">
                        <i class="fas fa-long-arrow-alt-left"></i>
                        No, back to role
                    </v-btn>
                </div>
                <div class="controls-right">
                    <v-btn dark color="red" type="submit">
                        <i class="far fa-trash-alt"></i>
                        Yes, delete role
                    </v-btn>
                </div>
            </div>

        </form>

    </div>
@stop