@extends("admin::layouts.master")

@section("content")

    <div class="wrapper">

        <!-- Breadcrumbs -->
        {!! Breadcrumbs::render("admin.roles") !!}

        <!-- Header -->
        <div class="page-header">
            <div class="page-header__text">
                <div class="page-header__title">Manage roles</div>
                <div class="page-header__subtitle">Although we all humans we can never be equal.</div>
            </div>
            <div class="page-header__actions">
                <v-btn small color="primary" href="{{ route('admin.roles.create') }}">
                    <i class="fas fa-plus"></i>
                    Create a new role
                </v-btn>
            </div>
        </div>
    
        <!-- Feedback -->
        @include("admin::partials.feedback")

        <!-- Content -->
        <data-table
            :data="{{ $roles->toJson() }}"
            :config="{{ $dataTableConfig }}">
        </data-table>

    </div>

@stop