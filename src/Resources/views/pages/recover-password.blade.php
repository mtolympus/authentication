@extends("auth::layouts.master")

@section("page_title")
    - Recover password
@stop

@section("content")

    <!-- Auth content card -->
    <div id="auth-content-card" class="elevation-24 @if(!$has_panel) no-panel @endif">

        <!-- Card left side -->
        @if ($has_panel)
            <div id="auth-content-card__left">
                <div id="auth-content-card__cinematic">
                    <div id="card-title">{{ $panel_title }}</div>
                    <div id="card-subtitle">{{ $panel_text }}</div>
                </div>
            </div>
        @endif

        <!-- Card right side -->
        <div id="auth-content-card__right" class="double-size">

            <!-- Form panel -->
            <div id="auth-content-card__form">
                
                <!-- Titles -->
                <h1 id="form-title">Recover your password</h1>
                <div id="form-subtitle">Fill in your email address and we'll send you a recovery link.</div>

                <!-- Feedback -->
                @include($feedbackPartial)
                
                <!-- The form -->
                <form action="{{ route('auth.recover-password.post') }}" method="post">
                    {{ csrf_field() }}
                    
                    <!-- Email address -->
                    <v-text-field label="Your e-mail address" name="email"></v-text-field>

                    <!-- Form controls -->
                    <div class="auth-form-controls margin-top">

                        <!-- Back button -->
                        <div class="auth-form-controls__left">
                            <v-btn flat small href="{{ route('auth.login') }}">
                                Back to login
                            </v-btn>
                        </div>
                        
                        <!-- Submit button -->
                        <div class="auth-form-controls__right">
                            <v-btn type="submit" color="primary">
                                Send recovery email!
                            </v-btn>
                        </div>

                    </div>

                </form>

            </div><!-- End of form panel -->

        </div><!-- End of card right side -->

    </div><!-- End of auth content card -->

@stop
