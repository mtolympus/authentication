@extends("auth::layouts.master")

@section("page_title")
    - Verify email
@stop

@section("content")
    <div id="auth-page">
    
        <!-- Page title -->
        <h2 class="page-title">Verify your email address</h2>

        <!-- Feedback -->
        @include($feedbackPartial)

        <!-- Form -->
        <v-card>
            <v-card-text>
                
                <!-- Introductory text -->
                <p>
                    Enter your email address and the received verification code and click on verify email
                </p>
                    
                <!-- Recover form -->
                <form action="{{ route('auth.recover-password.post') }}" method="post">
                    {{ csrf_field() }}
                    
                    <!-- Email address -->
                    <v-text-field label="Your e-mail address" name="email" value="{{ $email }}"></v-text-field>

                    <!-- Verification code -->
                    <v-text-field label="Verification code" name="code" value="{{ $code }}"></v-text-field>

                    <!-- Form controls -->
                    <div class="auth-form-controls">
                        <div class="auth-form-controls__left">
                            <v-btn flat href="{{ route('auth.login') }}">
                                Back to login
                            </v-btn>
                        </div>
                        <div class="auth-form-controls__right">
                            <v-btn type="submit" color="primary">
                                Verify my email!
                            </v-btn>
                        </div>
                    </div>

                </form>
            </v-card-text>
        </v-card>

    </div>
@stop
