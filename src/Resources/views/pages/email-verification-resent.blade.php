@extends("auth::layouts.master")

@section("page_title")
    - Email sent
@stop

@section("content")
    <div id="auth-content-card">
        <div id="auth-content-card__content">
            <div id="auth-content-card__message">
                <h1 id="message-title">
                    We've sent you another verification email!
                </h1>
                <div id="message-text">
                    Please check your inbox for the email: {{ $email }}, and click on the button you'll find inside.
                </div>
                <div id="message-icon">
                    <i class="fas fa-smile-beam"></i>
                </div>
                <div id="message-action">
                    <v-btn large flat color="primary" href="{{ $button_link }}">
                        {{ $button_text }}
                    </v-btn>
                </div>
            </div>
        </div>
    </div>
@stop