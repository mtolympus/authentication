@extends("auth::layouts.master")

@section("page_title")
    - Email sent
@stop

@section("content")

    <div id="auth-content-card">
        <div id="auth-content-card__content">
            <div id="auth-content-card__message">
                <h1 id="message-title">Recovery email has beent sent your way!</h1>
                <div id="message-text">
                    We've sent you an email containing a recovery link you can use to reset your password.
                </div>
                <div id="message-icon">
                    <i class="far fa-grin-beam"></i>
                </div>
                <div id="message-action">
                    <v-btn large flat href="{{ $button_link }}">
                        {{ $button_text }}
                    </v-btn>
                </div>
            </div>
        </div>
    </div>

@stop