@extends("auth::layouts.master")

@section("page_title")
    - 2FA Recovery
@stop

@section("content")

    <!-- Two factor authentication -->
    <div id="tfa" class="elevation-24">

        <!-- Header -->
        <div id="tfa__header">
            <h1 id="tfa__header-title">Two-step authentication</h1>
            <h2 id="tfa__header-subtitle">Recovery</h2>
        </div>
        
        <!-- Text -->
        <div id="tfa__text">
            Enter one of your recovery codes and your password to authenticate. 
            Afterwards you can reset and link another device if you need to.
        </div>

        <!-- Feedback -->
        @include($feedbackPartial)

        <!-- Form -->
        <form action="{{ route('auth.2fa.recovery.post') }}" method="post">
            {{ csrf_field() }}

            <!-- Code field -->
            <v-text-field 
                label="Code" 
                name="code" 
                class="mb-0" 
                hide-details
                browser-autocomplete="off">
            </v-text-field>

            <!-- Password field -->
            <div class="mt-3">
                <v-text-field
                    label="Your password"
                    name="password"
                    class="mb-0"
                    hide-details
                    type="password"
                    browser-autocomplete="off">
                </v-text-field>
            </div>

            <!-- Submit field -->
            <div class="mt-4">
                <v-btn block type="submit" color="primary">
                    <i class="fas fa-unlock-alt"></i>
                    Verify
                </v-btn>
            </div>
            
        </form>

    </div>

@stop
