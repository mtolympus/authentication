@extends("auth::layouts.master")

@section("page_title")
    - Setup 2 factor auth
@stop

@section("content")

    <div id="tfa-setup">

        <div id="tfa-setup__card" class="elevation-24">
            
            <div id="tfa-setup__card-content">

                <!-- Header -->
                <div id="tfa-setup__header">
                    <h1 id="tfa-setup__header-title">Two-step authentication</h1>
                    <div id="tfa-setup__header-subtitle">Adding an extra layer of security</div>
                </div>

                <!-- Image -->
                <div id="tfa-setup__card-image">
                    <img src="{{ asset('storage/images/auth/2fa.svg') }}" />
                </div>

                <!-- Feedback -->
                @include($feedbackPartial)
                
                <!-- Form -->
                <form action="{{ route('auth.2fa-setup.post') }}" method="post">
                    {{ csrf_field() }}

                    <!-- Setup wizard -->
                    <two-factor-setup 
                        qr-image-url="{{ $qrUrl }}"
                        :old-input="{{ $oldInput->toJson() }}">
                    </two-factor-setup>

                </form>
            
            </div>

            <!-- Footer with link to skip -->
            @if (!$required)
                <div id="tfa-setup__card-footer">
                    <a href="{{ $skipLink }}">
                        {{ $skipLinkText }}
                    </a>
                </div>
            @endif

        </div>

    </div>

@stop
