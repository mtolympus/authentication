@extends("auth::layouts.master")

@section("page_title")
    - 2FA
@stop

@section("content")

    <!-- Two factor authentication -->
    <div id="tfa" class="elevation-24">

        <!-- Header -->
        <div id="tfa__header">
            <h1 id="tfa__header-title">Two-step app verified!</h1>
        </div>
        
        <!-- Image -->
        <div id="tfa__image">
            <img src="{{ asset('storage/images/auth/2fa.svg') }}" />
        </div>
        
        <!-- Text -->
        <div id="tfa__text">
            To make sure you can still access your account in case you lose or forget your phone, write down the following recovery codes:
        </div>

        <!-- Recovery codes -->
        <div id="tfa__recovery-codes">
            @foreach ($recoveryCodes as $code)
                <div class="recovery-code">
                    {{ $code }}
                </div>
            @endforeach
        </div>

        <!-- Controls -->
        <div id="tfa__controls">
            <v-btn color="primary" href="{{ $redirectEndpoint }}">
                Continue
                <i class="fas fa-long-arrow-alt-right"></i>
            </v-btn>
        </div>


    </div>

@stop
