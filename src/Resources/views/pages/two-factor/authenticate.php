@extends("auth::layouts.master")

@section("page_title")
    - 2FA
@stop

@section("content")

    <!-- Two factor authentication -->
    <div id="tfa" class="elevation-24">

        <!-- Header -->
        <div id="tfa__header">
            <h1 id="tfa__header-title">Two-step authentication</h1>
        </div>
        
        <!-- Image -->
        <div id="tfa__image">
            <img src="{{ asset('storage/images/auth/2fa.svg') }}" />
        </div>
        
        <!-- Text -->
        <div id="tfa__text">
            Open the two-step authentication app on your mobile device to get your verification code.
        </div>

        <!-- Recovery -->
        @if ($recoveryAllowed)
            <div id="tfa__recovery-text">
                Don't have access to your mobile device? Enter a <a href="{{ route('auth.2fa.recovery') }}">recovery code</a>.
            </div>
        @endif

        <!-- Feedback -->
        @include($feedbackPartial)

        <!-- Form -->
        <form action="{{ route('auth.2fa.post') }}" method="post">
            {{ csrf_field() }}
            <div id="tfa__form">
                <div id="tfa__code-input">
                    <v-text-field 
                        label="Code" 
                        name="code" 
                        class="mb-0" 
                        hide-details
                        browser-autocomplete="off">
                    </v-text-field>
                </div>
                <div id="tfa__actions">
                    <v-btn type="submit" color="primary">
                        <i class="fas fa-unlock-alt"></i>
                        Verify
                    </v-btn>
                </div>
            </div>
        </form>

    </div>

@stop
