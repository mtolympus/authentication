@extends("auth::layouts.master")

@section("page_title")
    - Login
@stop

@section("content")

    <!-- Auth content card -->
    <div id="auth-content-card" class="elevation-24 @if(!$has_panel) no-panel @endif">

        <!-- Card left side -->
        @if ($has_panel)
            <div id="auth-content-card__left">
                <div id="auth-content-card__cinematic">
                    <div id="card-title">{{ $panel_title }}</div>
                    <div id="card-subtitle">{{ $panel_text }}</div>
                </div>
            </div>
        @endif

        <!-- Card right side -->
        <div id="auth-content-card__right">

            <!-- Form panel -->
            <div id="auth-content-card__form">
                
                <!-- Titles -->
                <h1 id="form-title">Login</h1>
                <div id="form-subtitle">Enter your credentials to gain access</div>

                <!-- Feedback -->
                @include($feedbackPartial)
                
                <!-- The form -->
                <form action="{{ route('auth.login.post') }}" method="post">
                    {{ csrf_field() }}
            
                    <!-- Email input -->
                    <v-text-field label="Email" name="email"></v-text-field>

                    <!-- Password input -->
                    <v-text-field label="Password" name="password" type="password"></v-text-field>

                    <!-- Form controls -->
                    <div class="auth-form-controls">

                        <!-- Remember me input -->
                        <div class="auth-form-controls__left">
                            <remember-me-input></remember-me-input>
                        </div>

                        <!-- Submit button -->
                        <div class="auth-form-controls__right">
                            <v-btn type="submit" large color="primary">
                                <i class="fas fa-sign-in-alt"></i> Login
                            </v-btn>
                        </div>
                        
                    </div>
            
                </form>

                <!-- Auth related actions -->
                @if ($registration_enabled or $recovery_enabled)
                    <div id="auth-actions">
                        @if ($registration_enabled)
                            <div class="auth-action">
                                <v-btn small flat href="{{ route('auth.register') }}">
                                    I don't have an account yet
                                </v-btn>
                            </div>
                        @endif
                        @if ($recovery_enabled)
                            <div class="auth-action right">
                                <v-btn small flat href="{{ route('auth.recover-password') }}">
                                    I forgot my password
                                </v-btn>
                            </div>
                        @endif
                    </div>
                @endif
                
            </div><!-- End of form panel -->

        </div><!-- End of card right side -->

    </div><!-- End of auth content card -->

@stop