@extends("auth::layouts.master")

@section("page_title")
    - Reset password
@stop

@section("content")

    <!-- Auth content card -->
    <div id="auth-content-card" class="elevation-24">

        <!-- Card left side -->
        <div id="auth-content-card__left">

            <!-- Cinematic panel -->
            <div id="auth-content-card__cinematic">
                <div id="card-title">Come to the dark side</div>
                <div id="card-subtitle">We have cookies</div>
            </div>
            
        </div><!-- End of card left side -->

        <!-- Card right side -->
        <div id="auth-content-card__right" class="double-size">

            <!-- Form panel -->
            <div id="auth-content-card__form">
                
                <!-- Titles -->
                <h1 id="form-title">Reset your password</h1>
                <div id="form-subtitle">Enter your new password and reset it.</div>

                <!-- Feedback -->
                @include($feedbackPartial)
                
                <!-- The form -->
                <form action="{{ route('auth.reset-password.post') }}" method="post">
                    {{ csrf_field() }}
                    
                    <!-- Email -->
                    <v-text-field label="Email" name="email" value="{{ $email }}"></v-text-field>

                    <!-- Code -->
                    <v-text-field label="Code" name="code" value="{{ $code }}"></v-text-field>
                    
                    <!-- Password -->
                    <v-text-field label="Password" name="password" type="password"></v-text-field>

                    <!-- Confirm password -->
                    <v-text-field label="Confirm password" name="password_confirmation" type="password"></v-text-field>
                    
                    <!-- Form controls -->
                    <div class="auth-form-controls">

                        <!-- Back button -->
                        <div class="auth-form-controls__left">
                            <v-btn flat small href="{{ route('auth.login') }}">
                                Back to login
                            </v-btn>
                        </div>

                        <!-- Submit button -->
                        <div class="auth-form-controls__right">
                            <v-btn type="submit" color="primary">
                                Reset my password
                            </v-btn>
                        </div>
                    </div>

                </form>

            </div><!-- End of form panel -->

        </div><!-- End of card right side -->

    </div><!-- End of auth content card -->

@stop
