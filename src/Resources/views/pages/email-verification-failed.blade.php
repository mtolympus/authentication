@extends("auth::layouts.master")

@section("page_title")
    - Email not verified
@stop

@section("content")
    <div id="auth-content-card">
        <div id="auth-content-card__content">
            <div id="auth-content-card__message">
                <h1 id="message-title">
                    Failed to verify your email address
                </h1>
                <div id="message-text">
                    {{ $error }}
                </div>
                <div id="message-icon">
                    <i class="fas fa-flushed"></i>
                </div>
                <div id="message-actions">
                    @if ($email_invalid)
                        <v-btn color="primary" large flat href="{{ $button_link }}">
                            {{ $button_text }}
                        </v-btn>
                    @else
                        <resend-verification-email-button
                            user-uuid="{{ $user->uuid }}"
                            api-endpoint="{{ route('api.auth.resend-verification-email') }}"
                            redirect-endpoint="{{ route('auth.verify-email.sent', $user->email) }}"
                            button-text="Send me a another verification email!">
                        </resend-verification-email-button>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop