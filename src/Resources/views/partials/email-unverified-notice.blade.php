<div id="unverified-email-notice">
    <div id="unverified-email-notice__icon">
        <i class="fas fa-exclamation-circle"></i>
    </div>
    <div id="unverified-email-notice__text">
        Your email address has not been verified yet! Check your email for an activation link.
        <a href="{{ route('auth.resend-verification-email', $email) }}">Haven't received your activation link?</a>
    </div>
</div>