<!-- Flash messages -->
@if (session('flash_notification', collect())->count() > 0)

    <!-- Conditional wrapper (open) -->
    @if (isset($wrapper) and $wrapper)
        <div class="alert-wrapper">
    @endif

        <!-- Flash messages -->
        @foreach (session('flash_notification', collect())->toArray() as $message)
            <div class="alert alert-{{ $message['level'] }} {{ $message['important'] ? 'alert-important' : '' }}" role="alert">
                {!! $message['message'] !!}
            </div>
        @endforeach
    
    <!-- Conditional wrapper (close) -->
    @if (isset($wrapper) and $wrapper)
        </div>
    @endif

    {{ session()->forget('flash_notification') }}

@endif

<!-- Form errors -->
@if ($errors->any())
    <div class="form-errors">
        <ul class="form-errors__list">
            @foreach ($errors->all() as $error)
                <li class="form-error">{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif