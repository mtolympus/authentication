@component('mail::message')
# Recover your account

Hey {{ $user->name }}, sorry to hear you lost your credentials. Follow the link below to reset your password.

@component('mail::button', [
    'url' => route('auth.reset-password', [
        'email' => $user->email, 
        'code' => $user->password_recovery_code
    ])
])
Reset your account's password
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
