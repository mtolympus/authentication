@component('mail::message')
# Account verification

Hey {{ $user->name }}, thanks for signing up with {{ config('app.name') }}!<br />
Follow the link below to verify your account.

@component('mail::button', [
    'url' => route('auth.verify-email', [
        'email' => $user->email, 
        'code' => $user->email_verification_code
    ])
])
Verify your account
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
a