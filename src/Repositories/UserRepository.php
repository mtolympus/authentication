<?php

namespace Hermes\Auth\Repositories;

use Hermes\Auth\Models\User;
use Prettus\Repository\Eloquent\BaseRepository;

class UserRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return "Hermes\\Auth\\Models\\User";
    }

    /**
     * Preload user
     * 
     * @param       Hermes\Auth\Models\User
     * @return      Hermes\Auth\Models\User
     */
    public function preload(User $user)
    {
        // Convert avatar to an absolute path
        $user->avatar = asset($user->avatar);

        // Add CRUD page links
        $user->view_href = route("admin.users.view", $user->slug);
        $user->edit_href = route("admin.users.edit", $user->slug);
        $user->delete_href = route("admin.users.delete", $user->slug);

        // Add formatted roles string
        $formatted_roles = "";
        if ($user->roles->count() > 0)
        {
            foreach ($user->roles as $role)
            {
                if ($formatted_roles != "") $formatted_roles .= ", ";
                $formatted_roles .= "<div class='role' style='background-color: ".$role->bg_color."; color: ".$role->text_color.";'>".$role->name."</div>";
            }
        }
        $user->formatted_roles = $formatted_roles;

        // Return preloaded User object
        return $user;
    }

    /**
     * All users for overview
     * 
     * @return      Illuminate\Support\Collection
     */
    public function allForOverview()
    {
        $out = [];

        $users = $this->all();

        if ($users->count() > 0)
        {
            foreach ($users as $user)
            {
                $out[] = $this->preload($user);
            }
        }

        return collect($out);
    }

    /**
     * Data table config (for overview)
     * 
     * @return      string              JSON encoded array
     */
    public function dataTableConfig()
    {
        return json_encode([
            "fields" => [
                [
                    "heading" => "Name",
                    "field" => "name",
                ],
                [
                    "heading" => "Email address",
                    "field" => "email"
                ]
            ]
        ]);
    }
}