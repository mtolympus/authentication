<?php

namespace Hermes\Auth\Repositories;

use Hermes\Auth\Models\Permission;
use Hermes\Auth\Models\PermissionGroup;
use Prettus\Repository\Eloquent\BaseRepository;

class PermissionRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return "Hermes\\Auth\\Models\\Permission";
    }

    public function preload(Permission $permission)
    {
        return $permission;
    }

    public function allForOverview()
    {
        $out = [];

        $all = $this->all();

        foreach ($all as $permission)
        {
            $out[] = $this->preload($permission);
        }

        return collect($out);
    }
}