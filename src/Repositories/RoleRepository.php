<?php

namespace Hermes\Auth\Repositories;

use Hermes\Auth\Models\Role;
use Hermes\Auth\Models\Permission;
use Prettus\Repository\Eloquent\BaseRepository;

class RoleRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return "Hermes\\Auth\\Models\\Role";
    }

    /**
     * Preload role
     * 
     * @param       Role
     * @return      Role
     */
    public function preload(Role $role)
    {
        // Preload role's permission
        $role->load("permissions");

        // Add a formatted version of the name
        $role->formatted_name = ucfirst($role->name);

        // Add number of users who have been assigned this role
        $num_users = $role->users->count();
        $role->num_users = $num_users." user".($num_users != 1 ? "s" : "");

        // Add number of permissions this role has been assigned
        $num_permissions = $role->permissions->count();
        $role->num_permissions = $num_permissions." permission".($num_permissions != 1 ? "s" : "");

        // Add CRUD route urls
        $role->view_href = route("admin.roles.view", $role->id);
        $role->edit_href = route("admin.roles.edit", $role->id);
        $role->delete_href = route("admin.roles.delete", $role->id);

        // Return preloaded role
        return $role;
    }

    /**
     * All roles for overview
     * 
     * @return      Collection
     */
    public function allForOverview()
    {
        $out = [];

        $roles = $this->all();

        if ($roles->count() > 0)
        {
            foreach ($roles as $role)
            {
                $out[] = $this->preload($role);
            }
        }

        return collect($out);
    }

    /**
     * Data table config (for overview)
     * 
     * @return      string              JSON encoded array
     */
    public function dataTableConfig()
    {
        return json_encode([
            "fields" => [
                [
                    "heading" => "Name",
                    "field" => "formatted_name"
                ],
                [
                    "heading" => "Num. users",
                    "field" => "num_users"
                ],
                [
                    "heading" => "Num. permissions",
                    "field" => "num_permissions"
                ]
            ]
        ]);
    }

    /**
     * Does the role have the given permission?
     * 
     * @param       Hermes\Auth\Models\Role                 The role we're checking
     * @param       Hermes\Auth\Models\Permission           The permission we're checking
     * @return      boolean
     */
    public function roleHasPermission(Role $role, Permission $permission)
    {
        // Grab all of the role's permissions
        $rolePermissions = $role->permissions;

        // If there are some to process
        if ($rolePermissions->count() > 0)
        {
            // Loop through each of them
            foreach ($rolePermissions as $rolePermission)
            {
                // If it's the permission we're looking for
                if ($rolePermission->id == $permission->id)
                {
                    return true;
                }
            }
        }

        // Aw
        return false;
    }

    /**
     * Does one of the provided role's have the given permission
     * 
     * @param       array<string>                           Array of role names
     * @param       Hermes\Auth\Models\Permission           The permission we're checking
     * @return      boolean
     */
    public function rolesContainPermission(array $roleNames, Permission $permission)
    {
        if (count($roleNames) > 0)
        {
            foreach ($roleNames as $roleName)
            {
                $role = $this->findWhere(["name" => $roleName])->first();
                if ($role)
                {
                    $rolePermissions = $role->permissions;
                    if ($rolePermissions->count() > 0)
                    {
                        foreach ($rolePermissions as $rolePermission)
                        {
                            if ($rolePermission->id == $permission->id)
                            {
                                return true;
                            }
                        }
                    }
                }
            }
        }

        return false;
    }
}