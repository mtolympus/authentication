<?php

namespace Hermes\Auth\Repositories;

use Hermes\Auth\Models\Permission;
use Hermes\Auth\Models\PermissionGroup;
use Prettus\Repository\Eloquent\BaseRepository;

class PermissionGroupRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return "Hermes\\Auth\\Models\\PermissionGroup";
    }
    
    public function preload(PermissionGroup $permissionGroup)
    {
        $permissionGroup->load("permissions");

        return $permissionGroup;
    }

    public function allForOverview()
    {
        $out = [];

        $groups = $this->all();
        
        $permissions = Permission::all();

        if ($groups->count() > 0)
        {
            foreach ($groups as $group)
            {
                $out[] = $this->preload($group);
            }
        }

        return collect($out);
    }
}