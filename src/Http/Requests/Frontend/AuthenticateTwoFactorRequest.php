<?php

namespace Hermes\Auth\Http\Requests\Frontend;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class AuthenticateTwoFactorRequest extends FormRequest
{
    public function authorize()
    {
        return Auth::check();
    }

    public function rules()
    {
        return [
            "code" => "required"
        ];
    }
}