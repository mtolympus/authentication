<?php

namespace Hermes\Auth\Http\Requests\Frontend;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class ResetPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "email" => "required|email|exists:users,email",
            "code" => "required",
            "password" => "required|confirmed",
            "password_confirmation" => "required"
        ];
    }
}
