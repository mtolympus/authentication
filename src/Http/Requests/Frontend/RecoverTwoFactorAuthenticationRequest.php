<?php

namespace Hermes\Auth\Http\Requests\Frontend;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class RecoverTwoFactorAuthenticationRequest extends FormRequest
{
    public function authorize()
    {
        return Auth::check();
    }

    public function rules()
    {
        return [
            "code" => "required",
            "password" => "required",
        ];
    }
    
    public function messages()
    {
        return [
            "code.required" => "A recovery code is required.",
            "password.required" => "Your password is required."
        ];
    }
}