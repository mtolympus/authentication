<?php

namespace Hermes\Auth\Http\Requests\Admin\Roles;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRoleRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "name" => "required",
            "icon" => "nullable",
            "bg_color" => "required",
            "text_color" => "required",
        ];
    }

    public function messages()
    {
        return [];
    }
}
