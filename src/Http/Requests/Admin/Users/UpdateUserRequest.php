<?php

namespace Hermes\Auth\Http\Requests\Admin\Users;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "roles" => "",
            "annotation" => "required",
            "first_name" => "required",
            "last_name" => "required",
            "email" => "required|email",
            "password" => "nullable|confirmed|min:8",
            "password_confirmation" => "nullable"
        ];
    }

    public function messages()
    {
        return [];
    }
}
