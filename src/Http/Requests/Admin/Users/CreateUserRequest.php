<?php

namespace Hermes\Auth\Http\Requests\Admin\Users;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "roles" => "",
            "annotation" => "required",
            "first_name" => "required",
            "last_name" => "required",
            "email" => "required|email|unique:users,email",
            "password" => "required|confirmed|min:8",
            "password_confirmation" => "required"
        ];
    }

    public function messages()
    {
        return [];
    }
}
