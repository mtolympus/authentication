<?php

namespace Hermes\Auth\Http\Controllers;

use Auth;
use Google2FA;
use Carbon\Carbon;

use Hermes\Auth\Models\User;
use Hermes\Auth\Events\User\LoggedIn;
use Hermes\Auth\Events\User\LoggedOut;
use Hermes\Auth\Events\User\Registered;
use Hermes\Auth\Events\User\RecoveredPassword;
use Hermes\Auth\Events\User\RequestedPasswordReset;
use Hermes\Auth\Jobs\SendVerifyAccountEmail;
use Hermes\Auth\Http\Requests\Frontend\LoginRequest;
use Hermes\Auth\Http\Requests\Frontend\RegisterRequest;
use Hermes\Auth\Http\Requests\Frontend\VerifyEmailRequest;
use Hermes\Auth\Http\Requests\Frontend\ResetPasswordRequest;
use Hermes\Auth\Http\Requests\Frontend\RecoverPasswordRequest;

use App\Http\Controllers\Controller;

/**
 * Authentication controller
 * 
 * This controller handles all the frontend requests
 * 
 * @author      Nick Verheijen <verheijen.webdevelopment@gmail.com>
 * @version     1.0.0
 */
class AuthController extends Controller
{
    // ---------------------------------------------
    // Login
    // ---------------------------------------------

    public function getLogin()
    {
        // Render the login page
        return view("auth::pages.login");
    }

    public function postLogin(LoginRequest $request)
    {
        // Attempt to login using the received credentials
        if (!Auth::attempt(["email" => $request->email, "password" => $request->password], (bool) $request->remember_me))
        {
            // If we failed, redirect back with errors
            return redirect()->back()->withInput()->withErrors(["password" => "Your password was incorrect :("]);
        }

        // Grab the user we just logged in with
        $user = Auth::user();

        // Fire event
        event(new LoggedIn($user));

        // Grab all the 2FA configuration params we need to decide where we want to redirect the user
        $tfa_enabled = config("authentication.2fa_enabled");
        $tfa_required = config("authentication.2fa_required");
        $tfa_promoting = config("authentication.2fa_promote_usage");
        
        // If 2FA is enabled but not required, the user has not setup 2FA and we're promoting 2FA usage
        if ($tfa_enabled and !$tfa_required and $tfa_promoting and !$user->google2fa_enabled)
        {
            // Redirect the user to the 2FA setup page
            return redirect()->route("auth.2fa-setup");
        }
        // In all other cases
        else
        {
            // Flash welcome message
            flash("Good to see you again ".$user->name."!")->success();

            // Redirect the user to configured post-login destination
            return redirect(parse_redirect_target(config("authentication.redirect_after_login")));
        }
    }   

    // ---------------------------------------------
    // Register
    // ---------------------------------------------
    
    public function getRegister()
    {
        // If registration is not allowed
        if (!config("authentication.registration_enabled"))
        {
            // Flash message and redirect back to the login
            flash(config("authentication.registration_disabled_text"))->error();
            return redirect()->route("auth.login");
        }

        // Render the register page
        return view("auth::pages.register");
    }
    
    public function postRegister(RegisterRequest $request)
    {
        // First we create the new user
        $user = User::create([
            "name" => $request->name,
            "email" => $request->email,
            "password" => bcrypt($request->password)
        ]);

        // Fire event
        event(new Registered($user));

        // Automatically login in the user afterwards
        Auth::login($user);
        
        // Flash success message
        flash("Awesome news ".$request->name."! Your account has been created and you've been logged in.")->success();

        // Redirect to configured post-register destination
        return redirect(parse_redirect_target(config("authentication.redirect_after_register")));
    }

    // ---------------------------------------------
    // Recover password
    // ---------------------------------------------
    
    public function getRecoverPassword()
    {
        // If registration is not allowed
        if (!config("authentication.recovery_enabled"))
        {
            // Flash message and redirect back to the login
            flash(config("authentication.recovery_disabled_text"))->error();
            return redirect()->route("auth.login");
        }

        // Render the recover password page
        return view("auth::pages.recover-password");
    }

    public function postRecoverPassword(RecoverPasswordRequest $request)
    {
        // Grab the user we are trying to recover the password for
        $user = User::where("email", $request->email)->first();
        
        // Fire the event which will trigger the job responsible for sending a reset email
        event(new RequestedPasswordReset($user));

        // Render the view informing the user we sent them an email
        return view("auth::pages.recovery-email-sent", [
            "user" => $user,
            "button_text" => config("authentication.recovery_email_button_text"),
            "button_link" => parse_redirect_target(config("authentication.recovery_email_button_link"))
        ]);
    }

    // ---------------------------------------------
    // Reset your password
    // ---------------------------------------------
    // The email you receive after requesting password recovery will redirect you to the routes below
    // ---------------------------------------------
    
    public function getResetPassword($email, $code)
    {
        return view("auth::pages.reset-password", [
            "email" => $email,
            "code" => $code
        ]);
    }
    
    public function postResetPassword(ResetPasswordRequest $request)
    {
        // Grab the user we're resetting the password for
        $user = User::where("email", $request->email)->first();

        // Validate the received recovery code
        if ($user->password_recovery_code != $request->code)
        {
            // If it's invalid; redirect
            return redirect()->back()->withInput()->withErrors(["code" => "You've entered an invalid password reset code."]);
        }

        // Reset the password
        $user->password = bcrypt($request->password);
        $user->save();

        // Fire event
        event(new RecoveredPassword($user));

        // Flash message & redirect to the login page
        flash("Successfully reset password! You should be able to login now.")->success();
        return redirect()->route("auth.login");
    }

    // ---------------------------------------------
    // Email verification
    // ---------------------------------------------
    
    /**
     * Verify email
     * 
     * If either the email or the verification code is invalid it will render the error page based on the error.
     * In one of the two possible failure cases the user will be presented a "retry" button which will redirect
     * the user to the "succesfully re-sent verification email" page.
     * 
     * If the email and verification code is valid, the successfully verified email page will be rendered.
     */
    public function getVerifyEmail($email, $code)
    {
        // Grab the user
        $user = User::where("email", $email)->first();

        // Make sure we got the user
        if (!$user)
        {
            // If the email address was invalid; display the error message
            return view("auth::pages.email-verification-failed", [
                "user" => $user,
                "email_invalid" => true,
                "button_text" => config("authentication.email_verification_failed_button_text"),
                "button_link" => parse_redirect_target(config("authentication.email_verification_failed_button_link")),
                "error" => "We could not activate your account because the email you provided was unknown to us! Please try again."
            ]);
        }

        // Validate the received verification code
        if ($user->email_verification_code != $code)
        {
            // If the code was invalid; display the error message
            return view("auth::pages.email-verification-failed", [
                "user" => $user,
                "email_invalid" => false,
                "error" => "We could not active your account because the verification code was invalid! Please try again."
            ]);
        }

        // Flag the user's email address as verified!
        $user->email_verified_at = Carbon::now();
        $user->save();
        
        // Render the email verification succeeded email!
        return view("auth::pages.email-verification-succeeded", [
            "user" => $user,
            "button_href" => parse_redirect_target(config("authentication.email_verification_button_link")),
            "button_text" => config("authentication.email_verification_button_text")
        ]);
    }

    /**
     * Verification email sent (again)
     * 
     * @param       string                                  $email
     * @return      View
     */
    public function getVerificationEmailSent($email)
    {
        // Grab the configured button details
        $button_text = config("authentication.verification_email_resent_button_text");
        $button_link = parse_redirect_target(config("authentication.verification_email_resent_button_link"));

        // Render the page
        return view("auth::pages.email-verification-resent", [
            "email" => $email,
            "button_text" => $button_text,
            "button_link" => $button_link
        ]);
    }

    /**
     * Resend verification email
     * Referenced in the unverified email partials
     * 
     * @param       string              The email address of the user we're resending a verification email
     * @return      View
     */
    public function getResendVerificationEmail($email)
    {
        // Grab the user
        $user = User::where("email", $email)->first();

        // Make sure we got it
        if (!$user)
        {
            flash("We could not find a user with the email '".$email."'")->error();
            return redirect()->route("home");
        }

        // Dispatch the job responsible for sending the email
        SendVerifyAccountEmail::dispatch($user);

        // Flash the success message
        flash("Successfully sent you another verification email!")->success();

        // Redirect back to the homepage
        return redirect()->route("home");
    }

    // ---------------------------------------------
    // Logout
    // ---------------------------------------------
    
    public function getLogout()
    {
        // Grab the user
        $user = Auth::user();

        // If we got a user
        if ($user)
        {
            // Fire event
            event(new LoggedOut($user));
    
            // Logout the user
            Auth::logout();
        }

        // If the user has 2FA enabled; logout 2FA as well
        if ($user->google2fa_enabled)
        {
            Google2FA::logout();
        }

        // Flash message
        flash("You have been logged out, see you next time!")->success();

        // Redirect to configured destination
        return redirect()->route("home");
    }
}