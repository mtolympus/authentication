<?php

namespace Hermes\Auth\Http\Controllers\Api;

use Hermes\Auth\Models\User;
use Hermes\Auth\Jobs\SendVerifyAccountEmail;
use Hermes\Auth\Http\Requests\Api\ResendVerifyEmailRequest;

use App\Http\Controllers\Controller;

/**
 * Authentication API controller
 * 
 * This controller handles all the API requests
 * 
 * @author      Nick Verheijen <verheijen.webdevelopment@gmail.com>
 * @version     1.0.0
 */
class AuthApiController extends Controller
{
    /**
     * POST: Resend verification email
     * 
     * This endpoint will be called dynamically by the ResendVerificationEmailButton.vue component
     * 
     * @param       ResendVerifyEmailRequest                $request
     * @return      JSON
     */
    public function postResendVerificationEmail(ResendVerifyEmailRequest $request)
    {
        // Grab the user
        $user = User::where("uuid", $request->uuid)->first();

        // Dispatch a job to send a new verification email to the user
        SendVerifyAccountEmail::dispatch($user);

        // Return a json response
        return response()->json(["success" => true]);
    }

}