<?php

namespace Hermes\Auth\Http\Controllers;

use Auth;
use Hash;
use Google2FA;
use RecoveryCodeGenerator;
use App\Http\Controllers\Controller;
use Hermes\Auth\Http\Requests\Frontend\AuthenticateTwoFactorRequest;
use Hermes\Auth\Http\Requests\Frontend\SetupTwoFactorAuthenticationRequest;
use Hermes\Auth\Http\Requests\Frontend\RecoverTwoFactorAuthenticationRequest;

class TwoFactorController extends Controller
{
    // --------------------------------------------------------
    // Setup 2FA
    // --------------------------------------------------------

    public function getSetup()
    {
        // If 2FA has been disabled; redirect
        if (!config("authentication.2fa_enabled"))
        {
            return redirect(parse_redirect_target(config("authentication.2fa_disabled_redirect")));
        }

        // Grab logged in user
        $user = Auth::user();

        // If user has already setup 2FA; redirect with a message
        if ($user->google2fa_enabled)
        {
            flash(config("authentication.2fa_already_enabled_text"))->error();
            return redirect(parse_redirect_target(config("authentication.2fa_already_setup_redirect")));
        }

        // If no 2FA secret has been generated for the user; generate & save it
        if (is_null($user->google2fa_secret))
        {
            $user->google2fa_secret = Google2FA::generateSecretKey(32);
            $user->save();
        }
        
        // Generate the QR code
        $qrUrl = Google2FA::getQRCodeInline(config("authentication.2fa_app_name"), $user->email, $user->google2fa_secret);

        // Render the page
        return view("auth::pages.two-factor.setup", [
            "user" => Auth::user(),
            "qrUrl" => $qrUrl,
            "required" => config("authentication.2fa_required"),
            "skipLink" => parse_redirect_target(config("authentication.2fa_authenticated_redirect")),
            "skipLinkText" => config("authentication.2fa_skip_link_text"),
            "oldInput" => collect([
                "submitted" => old("submitted")
            ])
        ]);
    }

    public function postSetup(SetupTwoFactorAuthenticationRequest $request)
    {
        // Grab the logged in user
        $user = Auth::user();

        // If the provided authentication code was invalid; redirect back with a message
        if (!Google2FA::verifyKey($user->google2fa_secret, $request->code))
        {
            flash(config("authentication.2fa_incorrect_code_text"))->error();
            return redirect()->route("auth.2fa-setup")->withInput();
        }

        // Check if the code has not been used before (anti sneaky peaky motherfuckers)
        $timestamp = Google2FA::verifyKeyNewer($user->google2fa_secret, $request->code, $user->google2fa_ts);

        // If the code is invalid; redirect with message
        if ($timestamp == false)
        {
            flash(config("authentication.2fa_used_code_text"))->error();
            return redirect()->route("auth.2fa-setup")->withInput();
        }

        // Authenticate the user
        Google2FA::login();

        // Determine if recovery is allowed
        $recovery_allowed = config("authentication.2fa_allow_recovery");

        // If recovery codes have been enabled
        if ($recovery_allowed)
        {
            // Generate recovery codes for the user & save them on the user
            $user->google2fa_recovery_codes = RecoveryCodeGenerator::generate(4)->toArray();
        }

        // Flag that 2fa has been set
        $user->google2fa_enabled = true;
        $user->google2fa_ts = $timestamp;
        $user->save();

        // If recovery codes have been enabled, redirect the user to the post-setup page
        if ($recovery_allowed)
        {
            return redirect()->route("auth.2fa-recovery-codes");   
        }
        // Redirect the user to the configured endpoint if recovery has been disabled
        else
        {
            return redirect(parse_redirect_target(config("authentication.2fa_setup_redirect")));
        }
    }

    public function getRecoveryCodes()
    {
        // Grab the logged in user
        $user = Auth::user();

        // Render the page
        return view("auth::pages.two-factor.recovery-codes", [
            "user" => $user,
            "recoveryCodes" => $user->google2fa_recovery_codes,
            "redirectEndpoint" => parse_redirect_target(config("authentication.2fa_setup_redirect"))
        ]);
    }
    
    // --------------------------------------------------------
    // Authenticate with 2FA
    // --------------------------------------------------------

    public function getAuthenticate()
    {
        // If 2FA has been disabled; redirect
        if (!config("authentication.2fa_enabled"))
        {
            return redirect(parse_redirect_target(config("authentication.2fa_disabled_redirect")));
        }

        // Render the page
        return view("auth::pages.two-factor.authenticate", [
            "user" => Auth::user(),
            "recoveryAllowed" => config("authentication.2fa_allow_recovery")
        ]);
    }

    public function postAuthenticate(AuthenticateTwoFactorRequest $request)
    {
        // Grab logged in user
        $user = Auth::user();

        // Validate the authentication code the user entered
        if (!Google2FA::verifyKey($user->google2fa_secret, $request->code))
        {
            flash(config("authentication.2fa_incorrect_code_text"))->error();
            return redirect()->route("auth.2fa");
        }

        // Check if the code has been used before
        $timestamp = Google2FA::verifyKeyNewer($user->google2fa_secret, $request->code, $user->google2fa_ts);

        // If the code is still valid (hasn't been used before)
        if ($timestamp !== false)
        {
            // Save the timestamp so we can check if it's used again
            $user->google2fa_ts = $timestamp;
            $user->save();
        }
        // If the code has been used before!
        else
        {
            flash(config("authentication.2fa_used_code_text"))->error();
            return redirect()->route("auth.2fa");
        }

        // If we've reached this point we're good, authenticate the user
        Google2FA::login();

        // We're authenticated, go to the homepage without a message
        return redirect(parse_redirect_target(config("authentication.2fa_authenticated_redirect")));
    }
    
    // --------------------------------------------------------
    // Authenticate with 2FA recovery code
    // --------------------------------------------------------

    public function getAuthenticateRecovery()
    {
        // If recovery has been disabled, redirect the user to the authentication page
        if (!config("authentication.2fa_allow_recovery"))
        {
            flash(config("authentication.2fa_recovery_disabled_text"))->error();
            return redirect()->route("auth.2fa");
        }

        // Render the page
        return view("auth::pages.two-factor.recover", [
            "oldInput" => collect([
                "code" => old("code")
            ])
        ]);
    }

    public function postAuthenticateRecovery(RecoverTwoFactorAuthenticationRequest $request)
    {
        // If recovery has been disabled, redirect the user to the authentication page
        if (!config("authentication.2fa_allow_recovery"))
        {
            flash(config("authentication.2fa_recovery_disabled_text"))->error();
            return redirect()->route("auth.2fa.recovery");
        }

        // Grab the logged in user
        $user = Auth::user();

        // Validate the password we received
        if (!Hash::check($request->password, $user->password))
        {
            flash(config("authenticate.2fa_recovery_invalid_password_text"));
            return redirect()->back()->withInput();
        }

        // Validate the recovery code we received
        if (!in_array($request->code, $user->google2fa_recovery_codes))
        {
            flash(config("authenticate.2fa_recovery_invalid_password_text"))->error();
            return redirect()->back();
        }

        // If all is good, remove the recovery code from the user's recovery codes
        $user->google2fa_recovery_codes = RecoveryCodeGenerator::purgeRecoveryCode($user->google2fa_recovery_codes, $request->code);
        $user->save();

        // If we've reached this point we're good, authenticate the user
        Google2FA::login();

        // We're authenticated, go to the homepage without a message
        return redirect(parse_redirect_target(config("authentication.2fa_authenticated_redirect")));
    }
}