<?php

namespace Hermes\Auth\Http\Controllers\Admin;

use Hermes\Auth\Repositories\RoleRepository;
use Hermes\Auth\Repositories\PermissionRepository;
use Hermes\Auth\Repositories\PermissionGroupRepository;

use Hermes\Admin\Http\Controllers\AdminController;

class PermissionController extends AdminController 
{
    private $roles;
    private $permissions;
    private $permissionGroups;

    public function __construct(PermissionRepository $permissionRepository, PermissionGroupRepository $permissionGroupRepository, RoleRepository $roleRepository)
    {
        parent::__construct();

        $this->roles = $roleRepository;
        $this->permissions = $permissionRepository;
        $this->permissionGroups = $permissionGroupRepository;
    }

    /**
     * Permission overview
     * 
     * @return      View
     */
    public function getOverview()
    {
        return view("auth::pages.admin.permissions.overview", [
            "roles" => $this->roles->allForOverview(),
            "permissionGroups" => $this->permissionGroups->allForOverview()
        ]);
    }
}