<?php

namespace Hermes\Auth\Http\Controllers\Admin;

use App\Models\User;

use Hermes\Auth\Models\Role;
use Hermes\Auth\Models\Permission;
use Hermes\Auth\Repositories\RoleRepository;
use Hermes\Auth\Repositories\PermissionRepository;
use Hermes\Auth\Repositories\PermissionGroupRepository;
use Hermes\Auth\Http\Requests\Admin\Roles\CreateRoleRequest;
use Hermes\Auth\Http\Requests\Admin\Roles\UpdateRoleRequest;
use Hermes\Auth\Http\Requests\Admin\Roles\DeleteRoleRequest;

use Hermes\Admin\Http\Controllers\AdminController;

class RoleController extends AdminController
{
    private $roles;
    private $permissions;
    private $permissionGroups;

    public function __construct(RoleRepository $roleRepository, PermissionRepository $permissionRepository, PermissionGroupRepository $permissionGroupRepository)
    {
        parent::__construct();
        
        $this->roles = $roleRepository;
        $this->permissions = $permissionRepository;
        $this->permissionGroups = $permissionGroupRepository;
    }

    /**
     * Role overview
     * 
     * @return      View
     */
    public function getOverview()
    {
        return view("auth::pages.admin.roles.overview", [
            "roles" => $this->roles->allForOverview(),
            "dataTableConfig" => $this->roles->dataTableConfig()
        ]);
    }

    /**
     * View role
     * 
     * @param       integer                             The ID of the role
     * @return      View
     */
    public function getView($id)
    {
        // Grab the role
        $role = $this->roles->find($id);
        if (!$role)
        {
            flash("Could not find the specified role")->error();
            return redirect()->route("admin.roles");
        }
        
        // Render the view role page
        return view("auth::pages.admin.roles.view", [
            "role" => $role,
            "permissionGroups" => $this->permissionGroups->allForOverview()
        ]);
    }

    /**
     * Create role
     * 
     * @return      View
     */
    public function getCreate()
    {
        // Render the create role page
        return view("auth::pages.admin.roles.create", [
            "permissions" => $this->permissions->allForOverview(),
            "permissionGroups" => $this->permissionGroups->allForOverview(),
            "oldInput" => collect([
                "name" => old("name"),
                "icon" => old("icon"),
                "bg_color" => old("bg_color"),
                "text_color" => old("text_color"),
            ]),
        ]);
    }

    /**
     * POST: Create role
     * 
     * @param       CreateRoleRequest                   The request
     * @return      Redirection
     */
    public function postCreate(CreateRoleRequest $request)
    {
        // Collect any selected permissions we should assign to the role
        $permissionsToAssign = [];
        foreach ($this->permissions->all() as $permission)
        {
            $name = str_replace(" ", "_", $permission->name);
            if ($request->has($name) and $request->input($name) == "true")
            {
                $permissionsToAssign[] = $permission;
            }
        }

        // Create the role
        $role = $this->roles->create([
            "name" => $request->name,
            "icon" => $request->icon,
            "bg_color" => $request->bg_color,
            "text_color" => $request->text_color,
        ]);
        
        // If there are permissions to assign
        if (count($permissionsToAssign) > 0)
        {
            // Loop through all of the permissions
            foreach ($permissionsToAssign as $permission)
            {
                // Assign the permission to the role
                $role->givePermissionTo($permission->name);
            }
        }

        // Flash message and redirect to view role page
        flash("Successfully created the role")->success();
        return redirect()->route("admin.roles.view", $role->id);
    }

    /**
     * Edit role
     * 
     * @param       integer                             ID of the role
     * @return      View
     */
    public function getEdit($id)
    {
        // Grab the role
        $role = $this->roles->find($id);
        if (!$role)
        {
            flash("Could not find the specified role")->error();
            return redirect()->route("admin.roles");
        }
        
        // Load all of the role's permissions
        $role->load("permissions");
        
        // Render the edit role page
        return view("auth::pages.admin.roles.edit", [
            "role" => $role,
            "permissions" => $this->permissions->allForOverview(),
            "permissionGroups" => $this->permissionGroups->allForOverview(),
            "oldInput" => collect([
                "name" => old("name"),
                "icon" => old("icon"),
                "bg_color" => old("bg_color"),
                "text_color" => old("text_color"),
            ]),
        ]);
    }

    /**
     * POST: Edit role
     * 
     * @param       UpdateRoleRequest                   The request
     * @param       integer                             ID of the role
     * @return      Redirection
     */
    public function postEdit(UpdateRoleRequest $request, $id)
    {
        // Grab the role
        $role = $this->roles->find($id);
        if (!$role)
        {
            flash("Could not find the specified role")->error();
            return redirect()->route("admin.roles");
        }

        // Update the role
        $role->name = $request->name;
        $role->icon = $request->icon;
        $role->bg_color = $request->bg_color;
        $role->text_color = $request->text_color;
        $role->save();

        // Collect any selected permissions we should assign to the role
        $permissionsToAssign = [];
        foreach ($this->permissions->all() as $permission)
        {
            $name = str_replace(" ", "_", $permission->name);
            if ($request->has($name) and $request->input($name) == "true")
            {
                $permissionsToAssign[] = $permission;
            }
        }

        // Sync the permissions on the role
        $role->syncPermissions($permissionsToAssign);
        
        // Flash message and redirect
        flash("Successfully saved changes to the role")->success();
        return redirect()->route("admin.roles.view", $role->id);
    }

    /**
     * Delete role
     * 
     * @param       integer                             ID of the role
     * @return      View
     */
    public function getDelete($id)
    {
        $role = $this->roles->find($id);
        if (!$role)
        {
            flash("Could not find the specified role")->error();
            return redirect()->route("admin.roles");
        }
        
        return view("auth::pages.admin.roles.delete", [
            "role" => $role,
        ]);
    }

    /**
     * POST: Delete role
     * 
     * @param       DeleteRoleRequest                   Request
     * @param       integer                             ID of the role
     * @return      Redirection
     */
    public function postDelete(DeleteRoleRequest $request, $id)
    {
        $role = $this->roles->find($id);
        if (!$role)
        {
            flash("Could not find the specified role")->error();
            return redirect()->route("admin.roles");
        }
        
        $role->delete();

        flash("Successfully deleted the role")->success();
        return redirect()->route("admin.roles");
    }
}