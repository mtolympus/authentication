<?php

namespace Hermes\Auth\Http\Controllers\Admin;

use App\Models\User;
use App\Http\Controllers\Controller;
use Hermes\Auth\Http\Requests\Admin\Users\CreateUserRequest;
use Hermes\Auth\Http\Requests\Admin\Users\UpdateUserRequest;
use Hermes\Auth\Http\Requests\Admin\Users\DeleteUserRequest;
use Hermes\Auth\Repositories\UserRepository;
use Hermes\Auth\Repositories\RoleRepository;
use Hermes\Auth\Repositories\PermissionRepository;
use Hermes\Auth\Repositories\PermissionGroupRepository;

class UserController extends Controller
{
    private $users;
    private $roles;
    private $permissions;
    private $permissionGroups;

    public function __construct(UserRepository $userRepository, RoleRepository $roleRepository, PermissionRepository $permissionRepository, PermissionGroupRepository $permissionGroupRepository)
    {
        $this->users = $userRepository;
        $this->roles = $roleRepository;
        $this->permissions = $permissionRepository;
        $this->permissionGroups = $permissionGroupRepository;
    }

    /**
     * User overview
     * 
     * @return          View
     */
    public function getOverview()
    {
        return view("auth::pages.admin.users.overview", [
            "users" => $this->users->allForOverview(),
            "dataTableConfig" => $this->users->dataTableConfig()
        ]);
    }

    /**
     * View user
     * 
     * @param           string                  Slug of the user
     * @return          View
     */
    public function getView($slug)
    {
        // Grab the user and make sure we get it
        $user = $this->users->findWhere(["slug" => $slug])->first();
        if (!$user)
        {
            flash("We could not find the requested user")->error();
            return redirect()->route("admin.users");
        }

        // dd($user->can('create users'));
        $user->load("roles");
        $user->load("roles.permissions");
        $user->load("permissions");

        // Render the user
        return view("auth::pages.admin.users.view", [
            "user" => $user,
            "permissionGroups" => $this->permissionGroups->allForOverview(),
        ]);
    }

    /**
     * Create user
     * 
     * @return          View
     */
    public function getCreate()
    {
        return view("auth::pages.admin.users.create", [
            "roles" => $this->roles->allForOverview(),
            "permissions" => $this->permissions->allForOverview(),
            "permissionGroups" => $this->permissionGroups->allForOverview(),
            "oldInput" => collect([
                "annotation" => old("annotation"),
                "first_name" => old("first_name"),
                "last_name" => old("last_name"),
                "email" => old("email"),
                "email_verified" => old("email_verified"),
            ])
        ]);
    }

    /**
     * POST: Create user
     * 
     * @param           CreateUserRequest               $request
     * @return          Redirection
     */
    public function postCreate(CreateUserRequest $request)
    {
        // Create the user
        $user = $this->users->create([
            "annotation" => $request->annotation,
            "first_name" => $request->first_name,
            "last_name" => $request->last_name,
            "name" => $request->first_name." ".$request->last_name,
            "email" => $request->email,
            "password" => bcrypt($request->password),
        ]);

        // Sync the selected roles
        $roles = json_decode($request->roles);
        $user->syncRoles($roles);

        // Collect any selected permissions we should assign to the user
        $permissionsToAssign = [];
        foreach ($this->permissions->all() as $permission)
        {
            $name = str_replace(" ", "_", $permission->name);
            if ($request->has($name) and $request->input($name) == "true") // and !$this->roles->rolesContainPermission($roles, $permission))
            { 
                $permissionsToAssign[] = $permission->name;
            }
        }

        // Sync the permissions on the user
        $user->givePermissionTo($permissionsToAssign);

        // Flash message and redirect
        flash("Successfully created the new user")->success();
        return redirect()->route("admin.users.view", $user->slug);
    }

    /**
     * Edit user
     * 
     * @param           string                          Slug of the user
     * @return          View
     */
    public function getEdit($slug)
    {
        // Grab the user & make sure we got it
        $user = $this->users->findWhere(["slug" => $slug])->first();
        if (!$user)
        {
            flash("We could not find the requested user")->error();
            return redirect()->route("admin.users");
        }

        // Preload all of the related relationships
        $user->load("roles");
        $user->load("roles.permissions");
        $user->load("permissions");

        // Render the edit user page
        return view("auth::pages.admin.users.edit", [
            "user" => $user,
            "roles" => $this->roles->allForOverview(),
            "permissions" => $this->permissions->allForOverview(),
            "permissionGroups" => $this->permissionGroups->allForOverview(),
            "oldInput" => collect([
                "annotation" => old("annotation"),
                "first_name" => old("first_name"),
                "last_name" => old("last_name"),
                "email" => old("email"),
                "email_verified" => old("email_verified")
            ])
        ]);
    }

    /**
     * POST: Edit user
     * 
     * @param           UpdateUserRequest               $request
     * @return          Redirection
     */
    public function postEdit(UpdateUserRequest $request, $slug)
    {
        // Grab the user and make sure we got it
        $user = $this->users->findWhere(["slug" => $slug])->first();
        if (!$user)
        {
            flash("We could not find the requested user")->error();
            return redirect()->route("admin.users");
        }
        
        // Update all of the user's details
        $user->annotation = $request->annotation;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->name = $request->first_name." ".$request->last_name;
        $user->email = $request->email;
        if ($request->password !== "" and $request->password_confirmation !== "" and $request->password == $request->password_confirmation)
        {
            $user->password = bcrypt($request->password);
        }
        $user->save();

        // Sync the roles
        $roles = json_decode($request->roles);
        $user->syncRoles($roles);

        // Collect any selected permissions we should assign to the role
        $permissionsToAssign = [];
        foreach ($this->permissions->all() as $permission)
        {
            $name = str_replace(" ", "_", $permission->name);
            if ($request->has($name) and $request->input($name) == "true" and !$this->roles->rolesContainPermission($roles, $permission))
            { 
                $permissionsToAssign[] = $permission->name;
            }
        }
        $user->syncPermissions($permissionsToAssign);

        // Flash message and redirect to the view user page
        flash("Successfully saved changes to the user")->success();
        return redirect()->route("admin.users.view", $user->slug);
    }

    /**
     * Delete user
     * 
     * @param           string                          Slug of the user
     * @return          View
     */
    public function getDelete($slug)
    {
        $user = $this->users->findWhere(["slug" => $slug])->first();
        if (!$user)
        {
            flash("We could not find the requested user")->error();
            return redirect()->route("admin.users");
        }

        return view("auth::pages.admin.users.delete", [
            "user" => $user
        ]);
    }

    /**
     * POST: Delete user
     * 
     * @param           DeleteUserRequest               $request
     * @return          Redirection
     */
    public function postDelete(DeleteUserRequest $request, $slug)
    {
        $user = $this->users->findWhere(["slug" => $slug])->first();
        if (!$user)
        {
            flash("We could not find the requested user")->error();
            return redirect()->route("admin.users");
        }

        $user->delete();

        flash("Successfully deleted the user")->success();
        return redirect()->route("admin.users");
    }
}