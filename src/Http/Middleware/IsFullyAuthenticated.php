<?php

namespace Hermes\Auth\Http\Middleware;

use Auth;
use Closure;
use Google2FA;
use PragmaRX\Google2FALaravel\Support\Authenticator;

class IsFullyAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // User should be authenticated; so if they're not..
        if (!Auth::check())
        {
            // Redirect with an error flash message
            flash(config("authentication.is_administrator_redirect_flash_message"))->error();
            return redirect()->route(\parse_redirect_target(config("authenticated.is_administrator_redirect")));
        }

        // Resolve an instance of the authenticator class of the Google2FA package
        $authenticator = app(Authenticator::class)->boot($request);
        
        // Grab the logged in user
        $user = Auth::user();

        // If 2FA is enabled
        if (config("authentication.2fa_enabled"))
        {
            // If 2FA is required & the user has not enabled 2FA; redirect to setup
            if (config("authentication.2fa_required") && !$user->google2fa_enabled)
            {
                return redirect()->route("auth.2fa-setup");
            }

            // If 2FA is enabled for the user but he/she has not authenticated yet; redirect to 2fa auth
            if ($user->google2fa_enabled and !$authenticator->isAuthenticated())
            {
                // Redirect the user to the 2FA gateway
                return redirect()->route("auth.2fa");   
            }
        }

        return $next($request);
    }
}
