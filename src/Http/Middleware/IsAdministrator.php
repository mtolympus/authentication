<?php

namespace Hermes\Auth\Http\Middleware;

use Auth;
use Closure;

class IsAdministrator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // If the user has not been authenticated
        if (!Auth::check())
        {
            // Redirect to login page with an error page
            flash("You're not authorized to view that section of the website.")->error();
            return redirect()->route("auth.login");
        }
        // If the user is authenticated
        else
        {
            // Grab the logged in user
            $user = Auth::user();

            // If the user is not an administrator
            if (!$user->hasRole('administrator'))
            {
                // Redirect to the homepage with an error
                flash("Only administrators can view that section of the website.")->error();
                return redirect()->route("home");
            }
        }

        return $next($request);
    }
}
