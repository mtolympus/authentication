<?php

namespace Hermes\Auth\Http\Middleware;

use Auth;
use Closure;

class IsGuest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // User should be a guest; so if the user is authenticated
        if (Auth::check())
        {
            // Redirect with an error flash message
            flash(config("authentication.is_guest_redirect_flash_message"))->error();
            return redirect(parse_redirect_target(config("authentication.is_guest_redirect")));
        }
        
        return $next($request);
    }
}
