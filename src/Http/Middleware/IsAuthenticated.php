<?php

namespace Hermes\Auth\Http\Middleware;

use Auth;
use Closure;

class IsAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // User should be authenticated; so if they're not..
        if (!Auth::check())
        {
            // Redirect with an error flash message
            flash(config("authentication.is_authenticated_redirect_flash_message"))->error();
            return redirect()->route(parse_redirect_target(config("authentication.is_authenticated_redirect")));
        }

        return $next($request);
    }
}
