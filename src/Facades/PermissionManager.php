<?php

namespace Hermes\Auth\Facades;

use Illuminate\Support\Facades\Facade;

class PermissionManager extends Facade
{
    protected static function getFacadeAccessor()
    {
        return "permissionManager";
    }
}