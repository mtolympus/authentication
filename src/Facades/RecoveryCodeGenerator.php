<?php

namespace Hermes\Auth\Facades;

use Illuminate\Support\Facades\Facade;

class RecoveryCodeGenerator extends Facade
{
    protected static function getFacadeAccessor()
    {
        return "recoveryCodeGenerator";
    }
}