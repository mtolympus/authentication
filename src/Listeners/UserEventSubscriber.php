<?php

namespace Hermes\Auth\Listeners;

use Hermes\Auth\Jobs\SendVerifyAccountEmail;
use Hermes\Auth\Jobs\SendRecoverPasswordEmail;

class UserEventSubscriber
{
    public function subscribe($events)
    {
        $events->listen('Hermes\Auth\Events\User\LoggedIn', 'Hermes\Auth\Listeners\UserEventSubscriber@onLogin');
        $events->listen('Hermes\Auth\Events\User\LoggedOut', 'Hermes\Auth\Listeners\UserEventSubscriber@onLogout');
        $events->listen('Hermes\Auth\Events\User\Registered', 'Hermes\Auth\Listeners\UserEventSubscriber@onRegister');
        $events->listen('Hermes\Auth\Events\User\PasswordRecovered', 'Hermes\Auth\Listeners\UserEventSubscriber@onPasswordRecovered');
        $events->listen('Hermes\Auth\Events\User\RequestedPasswordReset', 'Hermes\Auth\Listeners\UserEventSubscriber@onRequestedPasswordReset');
    }
    
    public function onLogin($event)
    {
        // Send notification to discord channel
    }

    public function onLogout($event)
    {
        // Send notification to discord channel
    }

    public function onRegister($event)
    {
        // Send notification to discord channel

        // Send the user an email with account details and a welcome message
        SendVerifyAccountEmail::dispatch($event->user);
    }

    public function onPasswordRecovered($event)
    {
        // Send notification to discord channel
    }

    public function onRequestedPasswordReset($event)
    {
        // Send notification to discord channel

        // Send the user an email with a password recovery link
        SendRecoverPasswordEmail::dispatch($event->user);
    }

}