<?php

namespace Hermes\Auth\Services;

class RecoveryCodeGenerator
{
    /**
     * Generate recovery codes
     * 
     * @param       integer                             Amount of codes to generate, defaults to 4
     * @return      Illuminate\Support\Collection       Collection of codes
     */
    public function generate(int $amount = 4, int $segments = 2, int $segmentLength = 8)
    {
        $out = [];

        for ($i = 0; $i < $amount; $i++)
        {
            $code = "";
            for ($j = 0; $j < $segments; $j++)
            {
                if ($code !== "") $code .= "-";
                $code .= $this->generateString($segmentLength);
            }
            $out[] = $code;
        }

        return collect($out);
    }

    /**
     * Generate a random string
     * 
     * @param       integer                             Amount of characters to generate
     * @return      string                              Randomly generated string
     */
    private function generateString($length = 16)
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $input_length = strlen($characters);
        $random_string = '';
        for($i = 0; $i < $length; $i++) {
            $random_character = $characters[mt_rand(0, $input_length - 1)];
            $random_string .= $random_character;
        }
        return $random_string;
    }

    /**
     * Purge a recovery code from a list of codes
     * 
     * @param       array               Codes to remove the code from
     * @param       string              Code to remove
     * @return      array               Codes without the removed code
     */
    public function purgeRecoveryCode($codes, $code)
    {
        if (($key = array_search($code, $codes)) !== false) {
            unset($codes[$key]);
        }
        return $codes;
    }
}