<?php

namespace Hermes\Auth\Services;

use Exception;

use Hermes\Auth\Models\Role;
use Hermes\Auth\Models\Permission;
use Hermes\Auth\Models\PermissionGroup;

class PermissionManager
{
    private $permissions;
    private $permissionGroups;

    public function __construct()
    {
        $this->permissions = Permission::all();
        $this->permissionGroups = PermissionGroup::all();
    }

    /**
     * Register permission groups
     * 
     * @param       array                           Array of permission groups data
     * @return      void
     */
    public function registerPermissionGroups(array $data)
    {
        if (count($data) > 0)
        {
            foreach ($data as $groupData)
            {
                $this->registerPermissionGroup($groupData);
            }
        }
    }

    /**
     * Register permission group
     * 
     * @param       array                           Array of permission group data
     * @return      PermissionGroup|false
     */
    public function registerPermissionGroup(array $data)
    {
        // If there is data to process
        if (count($data) > 0)
        {
            // Validate the data we received
            $requiredFields = ["name"];
            foreach ($requiredFields as $requiredField)
            {
                if (!array_key_exists($requiredField, $data))
                {
                    throw new Exception("Permission group is missing required field: ".$requiredField);
                }
            }

            // Attempt to grab the permission group by it's name
            $group = $this->getPermissionGroupByName($data["name"]);

            // If the group does not exist yet
            if (!$group)
            {
                // Compose group's data
                $groupData = ["name" => $data["name"]];
                if (array_key_exists("description", $data)) $groupData["description"] = $data["description"];

                // Create the permission group
                $group = PermissionGroup::create($groupData);
            }
            // If the group already exists
            else
            {
                // And if we also received the optional description field
                if (array_key_exists("description", $data))
                {
                    // Update the group's description using the received data
                    $group = $this->getPermissionGroupByName($data["name"]);
                    $group->description = $data["description"];
                    $group->save();
                }
            }

            // If the group came with permissions
            if (array_key_exists("permissions", $data) and count($data["permissions"]) > 0)
            {
                // Determine the roles to assign this permission to based on the 'assign_to_roles' property
                // we either did or didnt receive
                $roles = array_key_exists("assign_to_roles", $data) ? $data["assign_to_roles"] : null;
                
                // Register the permissions of this permission group
                $this->registerPermissions($data["permissions"], $group, $roles);
            }

            // Return the group
            return $group;
        }

        return false;
    }

    /**
     * Register permissions
     * 
     * @param       array                           Array of permissions data
     * @return      void
     */
    public function registerPermissions(array $data, PermissionGroup $group = null, array $roles = null)
    {
        // If there is data (permission entries) to process
        if (count($data) > 0)
        {
            // Loop through all of the data entries representing permissions
            foreach ($data as $permissionData)
            {
                // Register the permission
                $this->registerPermission($permissionData, $group, $roles);
            }
        }
    }

    /**
     * Register permission
     * 
     * @param       string                                  Name of the permission
     * @param       Hermes\Auth\Models\PermissionGroup      Name of the permission group the permission should be added to
     * @param       array                                   Array of role names to assign the permission to after creation
     * @return      void
     */
    public function registerPermission(string $name, PermissionGroup $group = null, array $roles = null)
    {
        // Attempt to grab the permission by it's name
        $permission = $this->getPermissionByName($name);

        // If the permission does not exists already
        if (!$permission)
        {
            // Composer the permission's data
            $permissionData = ["name" => $name];
    
            // If we received a PermissionGroup
            if (!is_null($group))
            {
                // Add it's ID to..
                $permissionData["permission_group_id"] = $group->id;
            }

            // Create the permission
            $permission = Permission::create($permissionData);
        }

        // If we should assign this permission to certain roles
        if (!is_null($roles) and count($roles) > 0)
        {
            // Loop through all of the role names
            foreach ($roles as $roleName)
            {
                // Attempt to find the role using it's name and if we do..
                $role = Role::where("name", $roleName)->first();
                if ($role)
                {
                    // Assign the permission to the role
                    $role->givePermissionTo($name);
                }
            }
        }

        return $permission;
    }

    /**
     * Permission group exists (by name)
     * 
     * @param       string                          Name of the permission group
     * @return      boolean
     */
    public function permissionGroupExistsByName(string $name)
    {
        if ($this->permissionGroups->count() > 0)
        {
            foreach ($this->permissionGroups as $group) 
            {
                if ($group->name == $name)
                {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Get permission group by name
     * 
     * @param       string                          The name of the permission group
     * @return      PermissionGroup                 Model instance
     */
    public function getPermissionGroupByName(string $name)
    {
        if ($this->permissionGroups->count() > 0)
        {
            foreach ($this->permissionGroups as $group)
            {
                if ($group->name == $name)
                {
                    return $group;
                }
            }
        }
        return false;
    }

    /**
     * Permission exists by name
     * 
     * @param       string                  Name of the permission
     * @return      boolean
     */
    public function permissionExistsByName(string $name)
    {
        if ($this->permissions->count() > 0)
        {
            foreach ($this->permissions as $permission)
            {
                if ($permission->name == $name)
                {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Get permission by name
     * 
     * @param       string                  Name of the permission
     * @return      Permission
     */
    public function getPermissionByName(string $name)
    {
        if ($this->permissions->count() > 0)
        {
            foreach ($this->permissions as $permission)
            {
                if ($permission->name == $name)
                {
                    return true;
                }
            }
        }
        return false;
    }
}