<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| These should probably go here, but since i'm using these endpoints from the frontend
| and I like the CSRF verification I place them here until I figure out how to add csrf
| to the api endpoint and not have it be a problem to have these endpoints in the api.php file
|
*/

// Resend verification email endpoint (used on verification failed page)
Route::post("resend-verification-email", "Api\AuthApiController@postResendVerificationEmail")->name("api.auth.resend-verification-email");

