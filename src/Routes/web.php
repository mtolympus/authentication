<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ---------------------------------------------------------------------
// Frontend
// ---------------------------------------------------------------------

// Auth routes that require the user to be unauthenticated
Route::group(["middleware" => "guest"], function() {

    // Login
    Route::get("login", "AuthController@getLogin")->name("auth.login");
    Route::post("login", "AuthController@postLogin")->name("auth.login.post");

    // Register
    Route::get("register", "AuthController@getRegister")->name("auth.register");
    Route::post("register", "AuthController@postRegister")->name("auth.register.post");

    // Recover password
    Route::get("recover-password", "AuthController@getRecoverPassword")->name("auth.recover-password");
    Route::post("recover-password", "AuthController@postRecoverPassword")->name("auth.recover-password.post");

    // Reset password
    Route::get("reset-password/{email}/{code}", "AuthController@getResetPassword")->name("auth.reset-password");
    Route::post("reset-password", "AuthController@postResetPassword")->name("auth.reset-password.post");

});

// Verify email
Route::get("verify-email/{email}/{code}", "AuthController@getVerifyEmail")->name("auth.verify-email");
Route::get("verification-email-sent/{email}", "AuthController@getVerificationEmailSent")->name("auth.verify-email.sent");

// Resend verification email endpoint (used in the unverified email notice)
Route::get("resend-verification-email/{email}", "AuthController@getResendVerificationEmail")->name("auth.resend-verification-email");

// Auth routes that require the user to be authenticated
Route::group(["middleware" => "auth"], function() {

    // Setup two factor authentication
    Route::get("setup-2fa", "TwoFactorController@getSetup")->name("auth.2fa-setup");
    Route::post("setup-2fa", "TwoFactorController@postSetup")->name("auth.2fa-setup.post");
    Route::get("setup-2fa/verified", "TwoFactorController@getRecoveryCodes")->name("auth.2fa-recovery-codes");

    // Two factor authentication
    Route::get("2fa", "TwoFactorController@getAuthenticate")->name("auth.2fa");
    Route::post("2fa", "TwoFactorController@postAuthenticate")->name("auth.2fa.post");

    // Two factor authentication using recovery codes
    Route::get("2fa-recovery", "TwoFactorController@getAuthenticateRecovery")->name("auth.2fa.recovery");
    Route::post("2fa-recovery", "TwoFactorController@postAuthenticateRecovery")->name("auth.2fa.recovery.post");

    // Logout
    Route::get("logout", "AuthController@getLogout")->name("auth.logout");

});

// ---------------------------------------------------------------------
// Admin
// ---------------------------------------------------------------------

if (config("authentication.register_admin_panel"))
{
    Route::group(["prefix" => "admin", "middleware" => ["auth.full", "admin"]], function() {

        // Manage users
        Route::group(["prefix" => "users"], function() {
            // Overview
            Route::get("/", "Admin\UserController@getOverview")->name("admin.users");
            // Create user
            Route::get("create", "Admin\UserController@getCreate")->name("admin.users.create");
            Route::post("create", "Admin\UserController@postCreate")->name("admin.users.create.post");
            // View user
            Route::get("{slug}", "Admin\UserController@getView")->name("admin.users.view");
            // Edit user
            Route::get("{slug}/edit", "Admin\UserController@getEdit")->name("admin.users.edit");
            Route::post("{slug}/edit", "Admin\UserController@postEdit")->name("admin.users.edit.post");
            // Delete user
            Route::get("{slug}/delete", "Admin\UserController@getDelete")->name("admin.users.delete");
            Route::post("{slug}/delete", "Admin\UserController@postDelete")->name("admin.users.delete.post");
        });
        
        // Manage roles
        Route::group(["prefix" => "roles"], function() {
            // Overview
            Route::get("/", "Admin\RoleController@getOverview")->name("admin.roles");
            // Create role
            Route::get("create", "Admin\RoleController@getCreate")->name("admin.roles.create");
            Route::post("create", "Admin\RoleController@postCreate")->name("admin.roles.create.post");
            // View role
            Route::get("{id}", "Admin\RoleController@getView")->name("admin.roles.view");
            // Edit role
            Route::get("{id}/edit", "Admin\RoleController@getEdit")->name("admin.roles.edit");
            Route::post("{id}/edit", "Admin\RoleController@postEdit")->name("admin.roles.edit.post");
            // Delete role
            Route::get("{id}/delete", "Admin\RoleController@getDelete")->name("admin.roles.delete");
            Route::post("{id}/delete", "Admin\RoleController@postDelete")->name("admin.roles.delete.post");
        });
        
        // Manage permissions
        Route::group(["prefix" => "permissions"], function() {
            // Overview
            Route::get("/", "Admin\PermissionController@getOverview")->name("admin.permissions");
        });

    });
}
