<?php

/**
 * Hermes Authentication Configuration
 * 
 * @version     1.0.0
 * @author      Nick Verheijen <verheijen.webdevelopment@gmail.com>
 */

return [

    // ------------------------------------------------------------------------
    // General authentication settings
    // ------------------------------------------------------------------------

    // Should the authentication package register itself with the Hermes\Admin package?
    'register_admin_panel' => false,

    // Partial to be used for displaying feedback on all authentication pages (this includes form errors & flash messages)?
    'feedback_partial' => 'auth::partials.feedback',

    // Base title for all authentication pages? Each page gets a unique suffix (Authentication - Login, for example)?
    'page_title' => 'Authentication',

    // Footer text
    'footer_text' => '© Copyrighted by Hermes the (e)Commerce god.2019 - 2020.',

    // ------------------------------------------------------------------------
    // Middleware
    // ------------------------------------------------------------------------
    
    // IsGuest middleware settings
    'is_guest_redirect' => ['type' => 'named_route', 'target' => 'dashboard'],
    'is_guest_redirect_flash_message' => "You can't view the requested page when you are logged in!",
    
    // IsAuthenticated middleware settings
    'is_authenticated_redirect' => ['type' => 'named_route', 'target' => 'home'],
    'is_authenticated_redirect_flash_message' => "You need to be logged in to view the requested page!",
    
    // IsFullyAuthenticated middleware settings
    'is_fully_authenticated_redirect' => ['type' => 'named_route', 'target' => 'home'],
    'is_fully_authenticated_redirect_flash_message' => "You need to be logged in to view the requested page!",
    
    // IsAdministrator middleware settings
    'is_administrator_redirect' => ['type' => 'named_route', 'target' => 'home'],
    'is_administrator_redirect_flash_message' => "You need to be logged in to view the requested page!",
    
    // ------------------------------------------------------------------------
    // Login
    // ------------------------------------------------------------------------
    
    // Where should we redirect the user after successful login?
    'redirect_after_login' => ['type' => 'named_route', 'target' => 'home'],

    // Login side panel
    'login_has_side_panel' => false,
    'login_side_panel_title' => 'Come to the dark side',
    'login_side_panel_subtitle' => 'We have cookies',
    
    // ------------------------------------------------------------------------
    // Registration
    // ------------------------------------------------------------------------
    
    // Toggle if registration is allowed
    'registration_enabled' => false,

    // Text to display in the flash message if registration has been disabled
    'registration_disabled_text' => 'Registration has been disabled.',

    // Where should we redirect the user after successful registration?
    'redirect_after_register' => ['type' => 'named_route', 'target' => 'home'],

    // Registration side panel
    'register_has_side_panel' => false,
    'register_side_panel_title' => 'Come to the dark side',
    'register_side_panel_subtitle' => 'We have cookies',

    // ------------------------------------------------------------------------
    // Account recovery
    // ------------------------------------------------------------------------

    // Toggle if account recovery is allowed
    'recovery_enabled' => false,

    // Text to display in the flash message if account recovery has been disabled
    'recovery_disabled_text' => 'Recovery has been disabled.',

    // Recovery side panel
    'recovery_has_side_panel' => false,
    'recovery_side_panel_title' => 'Come to the light side',
    'recovery_side_panel_subtitle' => 'We have happyness',
    
    // ------------------------------------------------------------------------
    // Email verification
    // ------------------------------------------------------------------------

    // Recovery email sent button 
    'recovery_email_button_text' => 'Back to the website',
    'recovery_email_button_link' => ['type' => 'named_route', 'target' => 'home'],

    // Email successfully verified page button
    'email_verification_button_link' => ['type' => 'named_route', 'target' => 'home'],
    'email_verification_button_text' => 'Let\'s get it on!',

    // Email failed to verify & email was invalid button
    'email_verification_failed_button_link' => ['type' => 'named_route', 'target' => 'home'],
    'email_verification_failed_button_text' => 'Back to the homepage',

    // Verification email resend button
    'verification_email_resent_button_link' => ['type' => 'named_route', 'target' => 'home'],
    'verification_email_resent_button_text' => 'Back to the homepage',

    // ------------------------------------------------------------------------
    // Two factor authentication
    // ------------------------------------------------------------------------
    
    // Toggle if two factor authentication is supported
    '2fa_enabled' => false,

    // Toggle if two factor authentication is required or not (will force users to set it up if true)
    '2fa_required' => true,

    // Toggle if account recovery using recovery codes is enabled or not
    '2fa_allow_recovery' => true,

    // Toggle if the user should be shown the 2FA setup page after logging in when 2FA has not
    // been setup by the user and 2FA is not required
    '2fa_promote_usage' => true,
    
    // App name to display on the user's 2FA authenticator application
    '2fa_app_name' => config('app.name'),

    // Skip setup link text; this link will be displayed at the bottom of the setup page if
    // 2fa is not required. The href of the link will be the '2fa_authenticated_redirect' route
    '2fa_skip_link_text' => 'I will configure my two factor authentication at a later time',
    
    // Message to display when someone tries to setup 2FA but already has it setup
    '2fa_already_enabled_text' => 'You have already setup two factor authentication.',
    
    // Message to display when someone tries to authenticate using 2FA but has entered an incorrect code
    '2fa_incorrect_code_text' => 'The authentication code you entered was incorrect.',
    
    // Message to display when someone tries to authenticate using 2FA but the code has already been used
    '2fa_used_code_text' => 'The authentication code you entered has expired.',

    // Message to display when someone tries to recover their two factor auth when it has been disabled
    '2fa_recovery_disabled_text' => 'Two factor authentication recovery has been disabled.',

    // Message to display when the user tries to recover their 2FA using an invalid password
    '2fa_recovery_invalid_password_text' => 'You have entered an invalid password.',

    // Where to redirect the user if he/she tried to visit a 2fa related page but 2fa was not enabled
    '2fa_disabled_redirect' => ['type' => 'named_route', 'target' => 'home'],

    // Where to redirect the user if he/she tried to setup but was already setup
    '2fa_already_setup_redirect' => ['type' => 'named_route', 'target' => 'home'],

    // Where to redirect the user after successfully setting up two factor authentication
    '2fa_setup_redirect' => ['type' => 'named_route', 'target' => 'home'],

    // Where to redirect the user after successfully authenticating using two factor authentication
    '2fa_authenticated_redirect' => ['type' => 'named_route', 'target' => 'home'],
    
];