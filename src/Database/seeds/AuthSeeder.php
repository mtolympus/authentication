<?php

use App\Models\User;

use Hermes\Settings\Models\Setting;
use Hermes\Settings\Models\SettingType;
use Hermes\Settings\Models\SettingGroup;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AuthSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->emptyDatabase();

        $this->createSettings();
        $this->createRolesAndPermissions();
        $this->createUsers();
    }

    /**
     * Empty database
     * 
     * @return      void
     */
    private function emptyDatabase()
    {
        DB::table("users")->delete();
        DB::table("roles")->delete();
        DB::table("permissions")->delete();
        DB::table("model_has_permissions")->delete();
        DB::table("model_has_roles")->delete();
        DB::table("role_has_permissions")->delete();
    }

    /**
     * Create settings
     * 
     * @return      void
     */
    private function createSettings()
    {
        // Grab the types
        $t_text = SettingType::where("name", "text")->first();
        $t_number = SettingType::where("name", "number")->first();
        $t_boolean = SettingType::where("name", "boolean")->first();
        $t_select = SettingType::where("name", "select")->first();
        $t_image = SettingType::where("name", "image")->first();
        $t_file = SettingType::where("name", "file")->first();
        
        // Group
        $auth = SettingGroup::create([
            "name" => "admin_auth_settings",
            "label" => "Authentication settings",
            "description" => "Configure the authentication package.",
        ]);

        // Settings
        $require_email_verification = Setting::create([
            "setting_group_id" => $auth->id,
            "setting_type_id" => $t_boolean->id,
            "name" => "admin_auth_require_email_verification",
            "label" => "Require email verification",
            "value" => false
        ]);
        $allow_registration = Setting::create([
            "setting_group_id" => $auth->id,
            "setting_type_id" => $t_boolean->id,
            "name" => "admin_auth_allow_registration",
            "label" => "Allow account registration",
            "value" => true
        ]);
        $allow_account_recovery = Setting::create([
            "setting_group_id" => $auth->id,
            "setting_type_id" => $t_boolean->id,
            "name" => "admin_auth_allow_account_recovery",
            "label" => "Allow account recovery",
            "value" => true
        ]);
        $enable_two_factor_auth = Setting::create([
            "setting_group_id" => $auth->id,
            "setting_type_id" => $t_boolean->id,
            "name" => "admin_auth_enable_two_factor",
            "label" => "Enable two factor authentication",
            "value" => true
        ]);
    }

    /**
     * Create roles and permissions
     * 
     * @return      void
     */
    private function createRolesAndPermissions()
    {
        // ----------------------------------------------------
        // Create permissions
        // ----------------------------------------------------

        // Manage users
        $add_users = Permission::create(["name" => "add users"]);
        $edit_users = Permission::create(["name" => "edit users"]);
        $delete_users = Permission::create(["name" => "delete users"]);
            
        // ----------------------------------------------------
        // Create roles & assign them some permissions
        // ----------------------------------------------------

        $admin = Role::create(["name" => "administrator"]);
        $admin->givePermissionto(["add users", "edit users", "delete users"]);

        $customer = Role::create(["name" => "customer"]);
    }

    /**
     * Create users
     * 
     * @return      void
     */
    private function createUsers()
    {
        $nick = User::create([
            "first_name" => "Nick",
            "last_name" => "Verheijen",
            "name" => "Nick Verheijen",
            "email" => "verheijen.webdevelopment@gmail.com",
            "password" => bcrypt("engeland"),
        ]);
        
        $nick->assignRole("administrator");
    }
}