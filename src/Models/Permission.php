<?php

namespace Hermes\Auth\Models;

use Spatie\Permission\Models\Permission as BasePermission;

class Permission extends BasePermission
{
    protected $fillable = [
        "permission_group_id",
        "name",
        "guard_name"
    ];

    public function permissionGroup()
    {
        return $this->belongsTo("Hermes\Auth\Models\PermissionGroup");
    }
}