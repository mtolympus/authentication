<?php

namespace Hermes\Auth\Models;

use Illuminate\Database\Eloquent\Model;

class PermissionGroup extends Model
{
    protected $table = "permission_groups";
    protected $guarded = ["id", "created_at", "updated_at"];
    protected $fillable = [
        "name",
        "description"
    ];

    public function permissions()
    {
        return $this->hasMany("Hermes\Auth\Models\Permission");
    }
}