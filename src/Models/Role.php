<?php

namespace Hermes\Auth\Models;

use Spatie\Permission\Models\Role as BaseRole;

class Role extends BaseRole
{
    protected $fillable = [
        "name",
        "guard_name",
        "text_color",
        "bg_color",
        "icon",
    ];
}