<?php

namespace Hermes\Auth\Models;

use Uuid;
use Carbon\Carbon;
use Spatie\Permission\Traits\HasRoles;
use Cviebrock\EloquentSluggable\Sluggable;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, HasRoles, Sluggable;

    protected $guard_name = 'web';

    protected $hidden = ['password', 'remember_token'];
    protected $fillable = [
        'uuid',
        'slug',
        'annotation',
        'name',
        'first_name',
        'last_name',
        'email',
        'email_verified',
        'email_verified_at',
        'email_verification_code',
        'password',
        'password_recovery_code',
        'google2fa_enabled',
        'google2fa_secret',
        'google2fa_ts',
        'google2fa_recovery_codes',
    ];
    protected $dates = [
        'email_verified_at'
    ];
    protected $casts = [
        'email_verified' => 'boolean',
        'google2fa_enabled' => 'boolean',
    ];

    // ----------------------------------------
    // Boot
    // ----------------------------------------

    public static function boot()
    {
        parent::boot();

        // Generate an UUID automatically when the model is created
        self::creating(function($model) {
            $model->uuid = (string) Uuid::generate(4);
        });
    }

    // ----------------------------------------
    // Sluggable configuration
    // ----------------------------------------

    public function sluggable()
    {
        return [
            "slug" => [
                "source" => "name"
            ]
        ];
    }

    // ----------------------------------------
    // Local scopes
    // ----------------------------------------
    
    public function scopeRegisteredToday($query)
    {
        return $query->whereDate("created_at", Carbon::now());
    }
    
    public function scopeRegisteredThisWeek($query)
    {
        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SUNDAY);
        $now = Carbon::now();
        return $query->whereYear("created_at", $now->year)
        ->whereMonth("created_at", $now->month)
        ->whereDay("created_at", ">=", $now->startOfWeek()->day)
        ->whereDay("created_at", "<=", $now->endOfWeek()->day);
    }
    
    public function scopeRegisteredThisMonth($query)
    {
        $now = Carbon::now();
        return $query->whereYear("created_at", $now->year)->whereMonth("created_at", $now->month);
    }
    
    public function scopeRegisteredThisYear($query)
    {
        return $query->whereYear("created_at", Carbon::now()->year);
    }
    
    // ----------------------------------------
    // Mutators
    // ----------------------------------------

    public function getGoogle2faRecoveryCodesAttribute($value)
    {
        return json_decode($value);
    }

    public function setGoogle2faRecoveryCodesAttribute($value)
    {
        $this->attributes["google2fa_recovery_codes"] = json_encode($value);
    }

    // ----------------------------------------
    // Utility methods
    // ----------------------------------------

    public function hasVerifiedEmail()
    {
        return $this->email_verified_at != null;
    }
}
