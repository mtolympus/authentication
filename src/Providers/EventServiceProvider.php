<?php

namespace Hermes\Auth\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [];

    protected $subscribe = [
        'Hermes\Auth\Listeners\UserEventSubscriber',
    ];
}