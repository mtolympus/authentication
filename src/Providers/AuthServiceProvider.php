<?php

namespace Hermes\Auth\Providers;

use AdminManager;

use Hermes\Auth\Services\PermissionManager;
use Hermes\Auth\Services\RecoveryCodeGenerator;

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Router $router)
    {
        // Middlewares
        $this->registerMiddlewares($router);
        
        // Register the package with the administration panel package
        if (config("authentication.register_admin_panel"))
        {
            $this->app->booted(function () {
                $this->registerAdmin();
            });
        }
    }
    
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Register route service provider
        $this->app->register(RouteServiceProvider::class);

        // Register event service provider
        $this->app->register(EventServiceProvider::class);

        // Register the permission manager service
        $this->app->singleton("permissionManager", function() {
            return new PermissionManager;
        });

        $this->app->singleton("recoveryCodeGenerator", function() {
            return new RecoveryCodeGenerator;
        });

        // Config file
        $this->registerConfiguration();

        // Database files
        $this->registerDatabase();

        // Views
        $this->registerViews();

        // Assets
        $this->registerAssets();
    }
    
    /**
     * Bootstrap middlewares
     * 
     * @return      void
     */
    private function registerMiddlewares(Router $router)
    {
        $router->aliasMiddleware("guest", "Hermes\Auth\Http\Middleware\IsGuest");
        $router->aliasMiddleware("auth", "Hermes\Auth\Http\Middleware\IsAuthenticated");
        $router->aliasMiddleware("auth.full", "Hermes\Auth\Http\Middleware\IsFullyAuthenticated");
        $router->aliasMiddleware("admin", "Hermes\Auth\Http\Middleware\IsAdministrator");
    }

    /**
     * Bootstrap configuration
     * Sets up merging & publishing of the config file
     * 
     * @return      void
     */
    private function registerConfiguration()
    {
        // Setup merging of the config file 
        $this->mergeConfigFrom(__DIR__."/../Config/config.php", "authentication");

        // Setup publishing of the config file
        $this->publishes([__DIR__."/../Config/config.php" => config_path("authentication.php")]);
    }
    
    /**
     * Bootstrap migrations
     * Sets up loading & publishing of the migration files
     * 
     * @return      void
     */
    private function registerDatabase()
    {
        // Setup migration loading
        $this->loadMigrationsFrom(__DIR__."/../Database/migrations");

        // Setup migration publishing in case modifications to the database structure need to be made.
        $this->publishes([__DIR__."/../Database/migrations" => database_path("migrations")], "database");

        // Publish the auth seeder
        $this->publishes([__DIR__."/../Database/seeds" => database_path("seeds")], "database");
    }

    /**
     * Bootstrap views
     * Sets up loading & publishing of the view files
     * 
     * @return      void
     */
    private function registerViews()
    {
        // Setup view loading
        $this->loadViewsFrom(__DIR__."/../Resources/views", "auth");

        // Setup view publishing
        $this->publishes([__DIR__."/../Resources/views" => resource_path("views/vendor/auth")], "views");

        // Authenication master layout
        View::composer("auth::layouts.master", function($view) {
            $view->with("page_title", config("authentication.page_title"));
            $view->with("footer_text", config("authentication.footer_text"));
        });

        // Compose all authentication pages that need to display feedback to have the right partial name to load
        $feedbackPartial = config("authentication.feedback_partial");
        
        // Login view composer
        View::composer("auth::pages.login", function($view) use ($feedbackPartial) {
            $view->with("feedbackPartial", $feedbackPartial);
            $view->with("has_panel", config("authentication.login_has_side_panel"));
            $view->with("panel_title", config("authentication.login_side_panel_title"));
            $view->with("panel_text", config("authentication.login_side_panel_subtitle"));
            $view->with("registration_enabled", config("authentication.registration_enabled"));
            $view->with("recovery_enabled", config("authentication.recovery_enabled"));
        });

        // Register view composer
        View::composer("auth::pages.register", function($view) use ($feedbackPartial) {
            $view->with("feedbackPartial", $feedbackPartial);
            $view->with("has_panel", config("authentication.register_has_side_panel"));
            $view->with("panel_title", config("authentication.register_side_panel_title"));
            $view->with("panel_text", config("authentication.register_side_panel_subtitle"));
        });

        // Recover password view composer
        View::composer("auth::pages.recover-password", function($view) use ($feedbackPartial) {
            $view->with("feedbackPartial", $feedbackPartial);
            $view->with("has_panel", config("authentication.recovery_has_side_panel"));
            $view->with("panel_title", config("authentication.recovery_side_panel_title"));
            $view->with("panel_text", config("authentication.recovery_side_panel_subtitle"));
        });

        // Reset password view composer
        View::composer("auth::pages.reset-password", function($view) use ($feedbackPartial) {
            $view->with("feedbackPartial", $feedbackPartial);
        });

        // Verify email view composer
        View::composer("auth::pages.verify-email", function($view) use ($feedbackPartial) {
            $view->with("feedbackPartial", $feedbackPartial);
        });

        // 2FA setup view composer
        View::composer("auth::pages.two-factor.setup", function($view) use ($feedbackPartial) {
            $view->with("feedbackPartial", $feedbackPartial);
        });

        // 2FA authentication view composer
        View::composer("auth::pages.two-factor.authenticate", function($view) use ($feedbackPartial) {
            $view->with("feedbackPartial", $feedbackPartial);
        });

        // 2FA recovery codes view composer
        View::composer("auth::pages.two-factor.recover-codes", function($view) use ($feedbackPartial) {
            $view->with("feedbackPartial", $feedbackPartial);
        });

        // 2FA recovery view composer
        View::composer("auth::pages.two-factor.recover", function($view) use ($feedbackPartial) {
            $view->with("feedbackPartial", $feedbackPartial);
        });
    }
    
    /**
     * Bootstrap the frontend assets
     * 
     * @return      void
     */
    private function registerAssets()
    {
        $this->publishes([
            __DIR__."/../Resources/assets/sass/auth.scss" => resource_path("sass/auth.scss"),
            __DIR__."/../Resources/assets/js/components" => resource_path("js/components/auth_module"),
            __DIR__."/../Resources/assets/js/event-bus.js" => resource_path("js/event-bus.js"),
            __DIR__."/../Resources/assets/images" => storage_path("app/public/images/auth")
        ], "assets");
    }
    
    /**
     * Register the admin panel extensions
     * 
     * @return      void
     */
    private function registerAdmin()
    {
        // Register the package with the admin panel module
        AdminManager::registerPackage("authentication", function($navigation, $permissions) {

            // Register a navigation header with some links
            $navigation->registerHeader([
                "order" => 2,
                "name" => "auth",
                "text" => "Authentication",
                "links" => [
                    [
                        "name" => "users",
                        "text" => "Users",
                        "icon" => "<i class='fas fa-users'></i>",
                        "href" => route("admin.users"),
                        "route" => "admin.users"
                    ],
                    [
                        "name" => "roles",
                        "text" => "Roles",
                        "icon" => "<i class='fas fa-pencil-ruler'></i>",
                        "href" => route("admin.roles"),
                        "route" => "admin.roles"
                    ],
                    [
                        "name" => "permissions",
                        "text" => "Permissions",
                        "icon" => "<i class='fas fa-check-double'></i>",
                        "href" => route("admin.permissions"),
                        "route" => "admin.permissions"
                    ],
                ]
            ]);

            // Register the package's permissions
            $permissions->registerPermissionGroups([
                [
                    "name" => "User permissions",
                    "description" => "All permission related to managing users of the application.",
                    "permissions" => [
                        "view users",
                        "create users",
                        "update users",
                        "delete users"
                    ],
                    "assign_to_roles" => [
                        "administrator"
                    ]
                ],
                [
                    "name" => "Role permissions",
                    "description" => "All permissions related to managing the roles of this application.",
                    "permissions" => [
                        "view roles",
                        "create roles",
                        "update roles",
                        "delete roles",
                    ]
                ],
            ]);

        });
    }
}