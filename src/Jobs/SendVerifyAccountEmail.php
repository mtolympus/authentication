<?php

namespace Hermes\Auth\Jobs;

use Mail;

use Hermes\Auth\Models\User;
use Hermes\Auth\Mail\VerifyAccount;

use Illuminate\Support\Str;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendVerifyAccountEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $user;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Generate a new recovery code for the user
        $this->user->email_verification_code = Str::random();
        $this->user->save();

        // Send the recover password to the user
        Mail::to($this->user)->send(new VerifyAccount($this->user));
    }
}
