<?php

namespace Hermes\Auth\Jobs;

use Mail;

use Hermes\Auth\Models\User;
use Hermes\Auth\Mail\RecoverPassword;

use Illuminate\Support\Str;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendRecoverPasswordEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function handle()
    {
        // Generate a new recovery code for the user
        $this->user->password_recovery_code = Str::random();
        $this->user->save();

        // Send the recover password to the user
        Mail::to($this->user)->send(new RecoverPassword($this->user));
    }
}
