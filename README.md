# Laravel Authentication

This package acts as a wrapper of the native authentication of Laravel and the spatie/laravel-permission package.
It was designed to provide advanced out of the box functionality to speed up the development time.

## Requirements

The package requires the following packages automatically upon installation:
- laracasts/flash
- webpatser/laravel-uuid
- spatie/laravel-permission

There should be a queue worker working on the background. All the jobs that this package runs will run on the default (no specified) queue.

## Installation

### Step 1. Install package and it's dependencies

Install the package and it's dependencies by running the following command:

```
composer require hermes/auth
```

### Step 2. Publish the packages' assets

Publish the spatie/laravel-permission migrations; by running the following command:

```
php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider" --tag="migrations"
```

> This package comes with a migration for your users table. So before continueing make sure you've deleted your default *xxxxxx_create_users_table.php* file.

The package comes loaded with a bunch of files; like migrations, a seeder, vue components, a sass stylesheet & a bunch of views. Publish all these assets by running:

```
php artisan vendor:publish --provider"Hermes\Auth\Providers\AuthServiceProvider"
```

The package also includes the spatie/permission package which we'll need to publish the assets of as well.

```
php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider" --tag="migrations"
```

### Step 3. Migrations & Seed

Run the migrations:

```
php artisan migrate
```

Setup the database seed & customize it to your needs before running it:

```
php artisan db:seed
```

### Step 4. Compiling the frontend styling

Add the auth.scss file to your *webpack.mix.js* file:

```
mix.sass('resources/sass/auth.scss', 'public/css')
```

### Step 5. Register Vue.js components

Load the vue components added in the *resources/js/components/auth_module* folder:

```
Vue.component('remember-me-input', require('./components/auth/RememberMeInput.vue'));
```

### Step 6. Update the User model

Replace your application's default User model with the following model and extend that one as needed:

```
<?php

namespace App\Models;

use Hermes\Auth\Models\User as BaseUser;

class User extends BaseUser
{
    protected $fillable = [
        "uuid",
        'annotation',
        'name',
        'email',
        'email_verified',
        'email_verified_at',
        'email_verification_code',
        'password',
        'password_recovery_code',
    ];
}
```

> Note: in the example above we've moved the User model to the App\Models namespace. If you decide to do the same (which we think you should) make sure you update the *app/config/auth.php* file to point to the right namespaced User model.

### Step 7. Update your application's middleware

Open up your *app/Http/Kernel.php* file and delete the default middlewares for authentication:

```
    'auth' => \App\Http\Middleware\Authenticate::class,
    'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
    'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
```

Replace them with the package's middleware:

```
    'auth' => \Hermes\Auth\Http\Middleware\IsAuthenticated::class,
    'guest' => \Hermes\Auth\Http\Middleware\IsGuest::class,
    'admin' => \Hermes\Auth\Http\Middleware\IsAdministrator::class,
```

...
...
...
...

Make sure your views include a way to display the flash messages from the *laracasts/flash* package. The auth views have a partial included by default.

## Usage

### User model

The user model should be extended if you want to modify it. The package's User model comes loaded with:

- uuid *complements the id, does not replace it*
- spatie/laravel-permissions's ```use HasRoles;``` trait
- spatie/laravel-permissions's ```protected $guard_name = "web";```
- local scope: registeredToday()
- local scope: registeredThisWeek()
- local scope: registeredThisMonth()

And no relationships. So you most likely will have to extend our model (& migration) and implement your extra fields & relationships yourselves.

### Events

The following events are fired by the package:
- Hermes\Auth\Events\User\LoggedIn;
- Hermes\Auth\Events\User\Registered;
- Hermes\Auth\Events\User\RecoveredPassword;
- Hermes\Auth\Events\User\RequestedPasswordReset;

The package has an EventSubscriber under the hood which listens to the above events and sends emails where needed.
You could listen for them yourself as well and do whatever you want (like send them to your Discord channel, like I do)

### Unverified email notice partial

The package comes with a partial *app/resources/views/vendor/auth/partials/email-unverified-notice.blade.php* for displaying a notice to the user when their email has not been verified yet. The partial is published along with the other views and can be customed as you see fit.

The partial has one required parameter **email** and can be included as following:

```
{{-- Email not verified notice --}}
@if (Auth::check() and !Auth::user()->hasVerifiedEmail())
    @include("auth::partials.email-unverified-notice", [
        "email" => $user->email
    ])
@endif
```

